
import React, {
	useContext,
	useEffect,
	useState
} from "react";
import {
	BigNumber,
	ChainType,
	ERC20Type,
	NFT,
	Web3,
	_AssetItem,
	_AssetType,
	addThousandSeparator,
	assetTypeToString,
	chainTypeToERC20,
	checkApprovalERC721,
	combineURLs,
	compactString,
	dateToStrWithMonth,
	getChainId,
	getDefaultWeb3,
	getERC20BalanceFromChain,
	getNullERC20,
	getUnixtimeFromBlock,
	localStorageGet,
	makeERC20Allowance,
	makeERC20AllowanceMultisig,
	removeThousandSeparator,
	requestTokenMetadataUpdate,
	setApprovalERC721,
	setApprovalERC721Multisig,
	tokenToFloat,
	tokenToInt,
	unixtimeToDate
} from "@envelop/envelop-client-core"
import CopyToClipboard from "react-copy-to-clipboard";
import TippyWrapper    from "../TippyWrapper";
import ApprovePopup    from "../ApprovePopup";
import TransferPopup   from "../TransferPopup";
import ExternalApps    from "../ExternalApps";
import TokenHistory    from "../TokenHistory";
import CoinSelector    from "../CoinSelector";
import OwnerSocials    from "../OwnerSocials";

import {
	AdvancedLoaderStageType,
	ERC20Context,
	InfoModalContext,
	Web3Context,
	_AdvancedLoadingStatus,
	_ModalTypes
} from "../../dispatchers";

import icon_i_copy         from '../../static/pics/i-copy.svg';
import icon_external_green from "../../static/pics/icons/i-external-green.svg";
import icon_link_green     from "../../static/pics/icons/i-link-green.svg";
import icon_dots_hor       from "../../static/pics/icons/i-dots-hor.svg";
import icon_send           from "../../static/pics/icons/i-send.svg";
import icon_i_download     from '../../static/pics/icons/i-download.svg';
import icon_logo           from "../../static/pics/logo-mob.svg";
import icon_i_del          from '../../static/pics/icons/i-del.svg';

import {
	NFTOrder,
	NFTOrderPrice,
	NFTOrderTypes,
	decodeNFTOrderType,
	getNFTOrders,
	makeNFTOrderBuy,
	makeNFTOrderBuyMultisig,
	makeNFTOrderSell,
	makeNFTOrderSellMultisig,
	removeNFTOrder,
	removeNFTOrderMultisig,
	takeNFTOrder,
	takeNFTOrderMultisig
} from "../../models/wrap";

import config                from '../../app.config.json';

type NFTViewPageProps = {
	token: NFT,
}

export default function NFTViewPage(props: NFTViewPageProps) {

	const { token } = props;

	const userMenuBlockRef   = React.createRef<HTMLDivElement>();
	const DESCRIPTION_LENGTH = 160;

	const [ transferPopup,     setTransferPopup     ] = useState(false);
	const [ approvePopup,      setApprovePopup      ] = useState(false);

	const [ menuOpened,        setMenuOpened        ] = useState(false);
	const [ cursorOnCardMenu,  setCursorOnCardMenu  ] = useState(false);
	const [ ownerBlockOpened,  setOwnerBlockOpened  ] = useState(false);
	const [ descriptionOpened, setDescriptionOpened ] = useState(false);

	const [ copiedHintWhere,  setCopiedHintWhere    ] = useState('');

	const [ tokenURIRequested, setTokenURIRequested ] = useState(false);

	const [ blockDate,         setBlockDate         ] = useState<BigNumber | undefined>(undefined);

	const [ orderManagerAddress,     setOrderManagerAddress     ] = useState<string | undefined>(undefined);
	const [ showOrderModal,          setShowOrderModal          ] = useState<string | undefined>(undefined);
	const [ inputOrderToken,         setInputOrderToken         ] = useState<string>('');
	const [ inputOrderAmount,        setInputOrderAmount        ] = useState<string>('');
	const [ orderList,               setOrderList               ] = useState<Array<NFTOrder>>([]);

	const [ isOwnerContract, setIsOwnerContract ] = useState(true);

	const {
		userAddress,
		currentChain,
		currentChainId,
		web3,
		getWeb3Force,
		availableChains,
	} = useContext(Web3Context);
	const {
		erc20List,
		requestERC20Token,
	} = useContext(ERC20Context);
	const {
		setLoading,
		unsetModal,
		setModal,
		createAdvancedLoader,
		updateStepAdvancedLoader,
	} = useContext(InfoModalContext);

	useEffect(() => {
		setIsOwnerContract(true);

		getDefaultWeb3(currentChainId).then(async (data) => {
			if ( !token || !token.owner ) { return; }
			if ( data ) {
				const code = await data.eth.getCode(token.owner);
				if ( code.toLowerCase() === '0x' ) { setIsOwnerContract(false); }
			}
		})
	}, [ currentChainId, token, userAddress ])

	const fetchOrdersList = async (orderManager: string) => {
		const tokenToFetch: _AssetItem = {
			asset: {
				assetType: token.assetType,
				contractAddress: token.contractAddress,
			},
			tokenId: token.tokenId,
			amount: '0',
		}
		const orders = await getNFTOrders(token.chainId, orderManager, tokenToFetch);
		setOrderList(orders);
	}
	useEffect(() => {
		const getOrdersParams = async () => {
			const foundChain = config.CHAIN_SPECIFIC_DATA.find((item) => { return item.chainId === token.chainId });
			if ( !foundChain ) { return; }

			setOrderManagerAddress(foundChain.orderManager);

			if ( foundChain.orderManager ) {
				console.log('orderManager', foundChain.orderManager);
				fetchOrdersList(foundChain.orderManager)
			}
		}

		getOrdersParams();

	}, [ token ]);

	useEffect(() => {
		if ( !token.blockNumber ) { return; }
		if ( !currentChain ) { return; }
		getUnixtimeFromBlock(currentChain.chainId, new BigNumber(token.blockNumber))
			.then((data) => { setBlockDate(data) })
	}, [ token ]);

	// ---------- COPYABLES ----------
	const copiedHintTimeout  = 2; // s
	let   copiedHintTimer    = 0;
	const getCopiedHint = (where: string) => {
		if ( copiedHintWhere === where  ) {
			return (<span className="btn-action-info">Copied</span>)
		}
	}
	// ---------- END COPYABLES ----------

	const getBreadcrumbs = () => {
		return (
			<div className="breadcrumbs">
				<a className="item-back btn btn-sm btn-gray" href="/dashboard">
					<svg className="mr-2" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M12 5.25H2.8725L7.065 1.0575L6 0L0 6L6 12L7.0575 10.9425L2.8725 6.75H12V5.25Z" fill="white"></path>
					</svg>
					<span>Back to the list</span>
				</a>
			</div>
		)
	}

	const getTransferButton = () => {
		if ( !userAddress ) { return null; }

		if (
			( token.assetType === _AssetType.ERC721  && !token.owner ) ||
			( token.assetType === _AssetType.ERC1155 && !token.owner )
		) {
			return (
				<TippyWrapper
					msg="Cannot check ownership"
				>
					<div className="btn-group">
							<button
								className="btn btn-md btn-gray btn-img"
								disabled={ true }
							>
								<img src={ icon_send } alt="" />
							</button>
					</div>
				</TippyWrapper>
			)
		}

		if ( token.assetType === _AssetType.ERC721  && token.owner  && token.owner.toLowerCase() !== userAddress.toLowerCase() ) { return null; }
		if ( token.assetType === _AssetType.ERC1155 && token.amount && token.amount.lte(0)                                     ) { return null; }
		return (
			<div className="btn-group">
				<button
					className="btn btn-md btn-gray btn-img"
					onClick={() => { setTransferPopup(true) }}
				>
					<img src={ icon_send } alt="" />
				</button>
			</div>
		)
	}
	const getExternalViewButton = () => {
		if ( !currentChain ) { return null; }

		return (
			<div className="btn-group">
				<a
					className="btn btn-md btn-gray btn-img"
					target="_blank" rel="noopener noreferrer"
					href={ combineURLs(currentChain.explorerBaseUrl, `/token/${token.contractAddress}?a=${token.tokenId}`) }
				>
					<img src={ icon_external_green } alt="" />
				</a>
			</div>
		)
	}
	const getCopyLinkButton = () => {
		if ( !currentChain ) { return null; }
		return (
			<div className="btn-group">
				<CopyToClipboard
					text={ `${window.location.origin}/token/${currentChain.chainId}/${token.contractAddress}/${token.tokenId}` }
					onCopy={() => {
						setCopiedHintWhere('toolbarbtn');
						clearTimeout(copiedHintTimer);
						copiedHintTimer = window.setTimeout(() => { setCopiedHintWhere(''); }, copiedHintTimeout*1000);
					}}
				>
					<button className="btn btn-md btn-gray btn-img">
						<img src={ icon_link_green } alt="" />
					</button>
				</CopyToClipboard>
				{ getCopiedHint('toolbarbtn') }
			</div>
		)
	}
	const openCardMenu = () => {
		setTimeout(() => {
			const body = document.querySelector('body');
			if ( !body ) { return; }
			body.onclick = (e: any) => {
				if ( !userMenuBlockRef.current ) { return; }
				const _path = e.composedPath() || e.path;
				if ( _path && _path.includes(userMenuBlockRef.current) ) { return; }
				closeCardMenu();
			};
		}, 100);
		setMenuOpened(true);
	}
	const closeCardMenu = () => {
		setTimeout(() => {
			if ( cursorOnCardMenu ) { return; }
			const body = document.querySelector('body');
			if ( !body ) { return; }
			body.onclick = null;
			setMenuOpened(false);
		}, 100);
	}
	const getMenuTokenApproveButton = () => {
		if ( !userAddress ) { return null; }
		if ( token.owner && token.owner.toLowerCase() !== userAddress.toLowerCase() ) { return null; }
		if ( token.amount && token.amount.lte(0) ) { return null; }

		return (
			<li>
				<button
					onClick={() => { setApprovePopup(true) }}
				>
					Approve
				</button>
			</li>
		)
	}
	const getMenuUpdateURIButton = () => {
		if ( !currentChain ) { return null; }
		if ( tokenURIRequested ) { return (
			<li><button>Request sent</button></li>
		) }

		return (
			<li>
				<button
					onClick={() => {
						setTokenURIRequested(true);
						requestTokenMetadataUpdate(currentChain.chainId, token.contractAddress, token.tokenId);
					}}
				>
					Request token data update
				</button>
			</li>
		)
	}
	const getMenuShareTwitterButton = () => {
		if ( !currentChain ) { return null; }

		return (
			<li>
				<button
					onClick={() => {
						window.open(`http://twitter.com/share?text=Explore this incredible NFT collection using DAO Envelop\'s tools on @Envelop_project&url=${(window as any).location.href}`, '_blank');
					}}
				>
					Share in Twitter
				</button>
			</li>
		)
	}
	const getMenu = () => {

		return (
			<div className="btn-dropdown">
				{
					menuOpened ?
					(
						<ul
							onMouseEnter={() => {
								openCardMenu();
								setCursorOnCardMenu(true);
							}}
							onMouseLeave={() => {
								closeCardMenu();
								setCursorOnCardMenu(false);
							}}
						>
							{ getMenuTokenApproveButton() }
							{ getMenuUpdateURIButton() }
							{ getMenuShareTwitterButton() }
						</ul>
					) : null
				}
			</div>
		)
	}
	const getMenuBtn = () => {
		if (
			!getMenuTokenApproveButton() &&
			!getMenuUpdateURIButton()
		) { return null; }

		return (
			<div
				className="btn-group"
				ref={ userMenuBlockRef }
				onMouseEnter={ openCardMenu }
				onMouseLeave={ closeCardMenu }
				onClick={ openCardMenu }
			>
				<button
					className={`btn btn-md btn-gray btn-img ${ menuOpened ? 'active' : ''}`}
				>
					<img src={ icon_dots_hor } alt="" />
				</button>
				{ getMenu() }
			</div>
		)
	}
	const getToolbar = () => {
		if ( !currentChain ) { return null; }

		return (
			<div className="col-lg-auto order-lg-2">
				<div className="wnft-header__actions">
					<div className="btns-group">

						{ getTransferButton() }
						{ getExternalViewButton() }

						{ getCopyLinkButton() }
						{ getMenuBtn() }
					</div>
				</div>
			</div>
		)
	}
	const getTypeBlock = () => {
		return (
			<>
				<div className="wnft-header__type white">
					<span className="text">NFT</span>
				</div>
				<div className="wnft-header__type white">
					{ assetTypeToString(token.assetType, currentChain?.EIPPrefix || 'ERC') }
				</div>
			</>
		)
	}
	const getHeaderInfo = () => {
		return (
			<div className="col-lg-auto order-lg-1">
				<div className="wnft-header__title">
					<CopyToClipboard
						text={ token.contractAddress }
						onCopy={() => {
							setCopiedHintWhere('headeraddress');
							clearTimeout(copiedHintTimer);
							copiedHintTimer = window.setTimeout(() => { setCopiedHintWhere(''); }, copiedHintTimeout*1000);
						}}
					>
						<button className="btn-copy">
							<TippyWrapper msg={ token.contractAddress }>
								<span>
									<span className="text-muted">ADDRESS</span>
									<b>{ compactString(token.contractAddress) }</b>
								</span>
							</TippyWrapper>
							<img src={icon_i_copy} alt="" />
							{ getCopiedHint('headeraddress') }
						</button>
					</CopyToClipboard>
					<CopyToClipboard
						text={ token.tokenId }
						onCopy={() => {
							setCopiedHintWhere('headerid');
							clearTimeout(copiedHintTimer);
							copiedHintTimer = window.setTimeout(() => { setCopiedHintWhere(''); }, copiedHintTimeout*1000);
						}}
					>
						<button className="btn-copy">
							<TippyWrapper msg={ token.tokenId }>
								<span>
									<span className="text-muted">ID </span>
									<b>{ compactString(token.tokenId) }</b>
								</span>
							</TippyWrapper>
							<img src={icon_i_copy} alt="" />
							{ getCopiedHint('headerid') }
						</button>
					</CopyToClipboard>
					<div className="wnft-header__extra">
						{ getTypeBlock() }
					</div>
				</div>
			</div>
		)
	}
	const getHeader = () => {
		return (
			<div className="wnft-header">
				<div className="row justify-content-lg-between">
					{ getToolbar() }
					{ getHeaderInfo() }
				</div>
			</div>
		)
	}

	const removeOrderSubmit = async (orderId: string, _currentChain: ChainType, _web3: Web3, _userAddress: string, isMultisig?: boolean) => {

		if ( !orderManagerAddress ) { return; }

		setLoading('Waiting to remove order');

		const tokenToRemove: _AssetItem = {
			asset: {
				assetType: token.assetType,
				contractAddress: token.contractAddress,
			},
			tokenId: token.tokenId,
			amount: '0',
		}

		let txResp: any;
		try {
			if ( isMultisig ) {
				txResp = await removeNFTOrderMultisig(_web3, orderManagerAddress, orderId, tokenToRemove);
			} else {
				txResp = await removeNFTOrder(_web3, orderManagerAddress, orderId, tokenToRemove);
			}
		} catch(e: any) {
			setModal({
				type: _ModalTypes.error,
				title: `Cannot remove order`,
				details: [
					`Token: ${token.contractAddress}, ${token.tokenId}`,
					`User address: ${_userAddress}`,
					`Order id: ${orderId}`,
					'',
					e.message || e
				]
			});
			return;
		}

		if ( isMultisig ) {
			setModal({
				type: _ModalTypes.success,
				title: `Transaction has been created`,
				buttons: [{
					text: 'Ok',
					clickFunc: () => {
						unsetModal();
						setShowOrderModal(undefined);
						fetchOrdersList(orderManagerAddress);
					}
				}],
				links: [{
					text: `View on ${_currentChain.explorerName}`,
					url: combineURLs(_currentChain.explorerBaseUrl, `/tx/${txResp.transactionHash}`)
				}],
			});
		} else {
			setModal({
				type: _ModalTypes.success,
				title: `Order successfully removed`,
				buttons: [{
					text: 'Ok',
					clickFunc: () => {
						unsetModal();
						setShowOrderModal(undefined);
						fetchOrdersList(orderManagerAddress);
					}
				}],
				links: [{
					text: `View on ${_currentChain.explorerName}`,
					url: combineURLs(_currentChain.explorerBaseUrl, `/tx/${txResp.transactionHash}`)
				}],
			});
		}
	}
	const getOrderRowRemoveBtn = (item: NFTOrder) => {
		if ( !userAddress ) { return null; }

		if ( token.owner && token.owner.toLowerCase() === userAddress.toLowerCase() ) {
			return (
				<>
				<div className="mb-2 col-4 col-md-auto">
					<button
						className="btn btn-sm btn-gray btn-img w-100"
						onClick={async () => {
							if ( !currentChain ) { return; }

							let _web3 = web3;
							let _userAddress = userAddress;
							let _currentChain = currentChain;

							if ( !web3 || !userAddress || await getChainId(web3) !== _currentChain.chainId ) {
								setLoading('Waiting for wallet');

								try {
									const web3Params = await getWeb3Force(_currentChain.chainId);

									_web3 = web3Params.web3;
									_userAddress = web3Params.userAddress;
									_currentChain = web3Params.chain;
									// unsetModal();
								} catch(e: any) {
									setModal({
										type: _ModalTypes.error,
										title: 'Error with wallet',
										details: [
											e.message || e
										]
									});
									return;
								}
							}

							if ( !_web3 || !_userAddress || !_currentChain ) { return; }
							const isMultisig = localStorageGet('authMethod').toLowerCase() === 'safe';
							removeOrderSubmit(item.orderId, _currentChain, _web3, _userAddress, isMultisig);
						}}
					>
						<img src={ icon_i_del } alt="" />
					</button>
				</div>
				<div className="mb-2 col col-md-auto md-right">
					<button
						className="btn btn-sm btn-white w-100"
						onClick={async () => {
							if ( !currentChain ) { return; }

							let _web3 = web3;
							let _userAddress = userAddress;
							let _currentChain = currentChain;

							if ( !web3 || !userAddress || await getChainId(web3) !== _currentChain.chainId ) {
								setLoading('Waiting for wallet');

								try {
									const web3Params = await getWeb3Force(_currentChain.chainId);

									_web3 = web3Params.web3;
									_userAddress = web3Params.userAddress;
									_currentChain = web3Params.chain;
									// unsetModal();
								} catch(e: any) {
									setModal({
										type: _ModalTypes.error,
										title: 'Error with wallet',
										details: [
											e.message || e
										]
									});
									return;
								}
							}

							if ( !_web3 || !_userAddress || !_currentChain ) { return; }
							const isMultisig = localStorageGet('authMethod').toLowerCase() === 'safe';
							takeOrderSubmit(item, _currentChain, _web3, _userAddress, isMultisig);
						}}
					>Accept</button>
				</div>
				</>
			)
		}

		if ( item.orderMaker.toLowerCase() === userAddress.toLowerCase() ) {
			return (
				<div className="mb-2 col-4 col-md-auto">
					<button
						className="btn btn-sm btn-gray btn-img w-100"
						onClick={async () => {
							if ( !currentChain ) { return; }

							let _web3 = web3;
							let _userAddress = userAddress;
							let _currentChain = currentChain;

							if ( !web3 || !userAddress || await getChainId(web3) !== _currentChain.chainId ) {
								setLoading('Waiting for wallet');

								try {
									const web3Params = await getWeb3Force(_currentChain.chainId);

									_web3 = web3Params.web3;
									_userAddress = web3Params.userAddress;
									_currentChain = web3Params.chain;
									// unsetModal();
								} catch(e: any) {
									setModal({
										type: _ModalTypes.error,
										title: 'Error with wallet',
										details: [
											e.message || e
										]
									});
									return;
								}
							}

							if ( !_web3 || !_userAddress || !_currentChain ) { return; }
							const isMultisig = localStorageGet('authMethod').toLowerCase() === 'safe';
							removeOrderSubmit(item.orderId, _currentChain, _web3, _userAddress, isMultisig);
						}}
					>
						<img src={ icon_i_del } alt="" />
					</button>
				</div>
			)
		}

		return null;
	}
	const getOrderRow = (item: NFTOrder) => {

		let tokenToRender: ERC20Type = getNullERC20(item.price.payToken);
		if ( item.price.payToken.toLowerCase() === '0x0000000000000000000000000000000000000000' ) {
			const foundChain = availableChains.find((iitem) => { return iitem.chainId === token.chainId });
			if ( foundChain ) { tokenToRender = chainTypeToERC20(foundChain); }
		} else {
			const foundToken = erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === item.price.payToken.toLowerCase() });
			if ( foundToken ) { tokenToRender = foundToken; }
		}

		const amountToRender = tokenToFloat(new BigNumber(item.price.payAmount), tokenToRender.decimals).toString()

		return (
			<div className="item" key={item.orderId}>
			<div className="row">
				<div className="mb-2 col-3 col-md-1">#{ parseInt(item.orderId) }</div>
				<div className="mb-2 col-12 col-md-auto">
					<span className="col-legend">Maker: </span>
					<TippyWrapper msg={ item.orderMaker } >
						<span className="text-break">{ compactString(item.orderMaker) }</span>
					</TippyWrapper>
				</div>
				<div className="mb-2 col-12 col-md">
					<span className="col-legend">Price: </span>
					<span className="text-break">{ amountToRender } { ' ' } { tokenToRender.symbol }</span>
				</div>
				{/* <div className="mb-2 col-12 col-md-auto">
					<span className="text-break text-muted"><small>02.09.2024 13:00</small></span>
				</div> */}
				{ getOrderRowRemoveBtn(item) }
			</div>
			</div>
		)
	}

	const getOrdersBlockOwnerHeader = () => {
		const sellOrder = orderList.find((item) => { return `${item.orderType}` === `${NFTOrderTypes.SELL}` });
		let sellOrderFoundToken: ERC20Type = getNullERC20(inputOrderToken);
		let sellOrderAmount: string = '0';

		if ( sellOrder ) {
			const foundToken = erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === sellOrder.price.payToken.toLowerCase() });
			if ( foundToken ) {
				sellOrderFoundToken = foundToken;
			}
			sellOrderAmount = tokenToFloat(new BigNumber(sellOrder.price.payAmount), sellOrderFoundToken.decimals).toString();
		}

		if (
			userAddress &&
			token.owner &&
			token.owner.toLowerCase() === userAddress.toLowerCase() &&
			sellOrder
		) {
			return (
				<>
				<div className="d-flex align-items-center flex-wrap">
					<div className="h4">Offers</div>
					{/* <span className="py-2"><span className="text-muted">Your price</span> { sellOrderAmount } { sellOrderFoundToken.symbol }</span> */}
				</div>
				<div className="d-flex align-self-start align-self-sm-center">
					<button
						className="btn btn-md btn-gray"
						onClick={async () => {
							if ( !currentChain ) { return; }

							let _web3 = web3;
							let _userAddress = userAddress;
							let _currentChain = currentChain;

							if ( !web3 || !userAddress || await getChainId(web3) !== _currentChain.chainId ) {
								setLoading('Waiting for wallet');

								try {
									const web3Params = await getWeb3Force(_currentChain.chainId);

									_web3 = web3Params.web3;
									_userAddress = web3Params.userAddress;
									_currentChain = web3Params.chain;
									// unsetModal();
								} catch(e: any) {
									setModal({
										type: _ModalTypes.error,
										title: 'Error with wallet',
										details: [
											e.message || e
										]
									});
									return;
								}
							}

							if ( !_web3 || !_userAddress || !_currentChain ) { return; }
							const isMultisig = localStorageGet('authMethod').toLowerCase() === 'safe';
							removeOrderSubmit(sellOrder.orderId, _currentChain, _web3, _userAddress, isMultisig);
						}}
					>Cancel<span className="d-none d-lg-inline">&nbsp;sale</span></button>
				</div>
				</>
			)
		}

		return (
			<div className="d-flex align-items-center flex-wrap">
				<div className="h4">Offers</div>
			</div>
		)

	}
	const getOrdersBlock = () => {
		if ( token.assetType !== _AssetType.ERC721 ) { return null; }
		if ( !orderManagerAddress ) { return; }

		const buyOrder = orderList.find((item) => { return decodeNFTOrderType(item.orderType) === NFTOrderTypes.BUY });
		if ( !buyOrder ) { return null; }

		return (
			<div className="c-wrap">
				<div className="c-wrap__header">
					{ getOrdersBlockOwnerHeader() }
				</div>
				<div className="c-wrap__table mt-3">

					<div className="item item-header">
					<div className="row">
						<div className="mb-2 col-12 col-md-1">Id</div>
						<div className="mb-2 col-12 col-md-2">Maker</div>
						<div className="mb-2 col-12 col-md-auto">Price</div>
					</div>
					</div>

					{
						orderList
							.filter((item) => { return `${item.orderType}` === `${NFTOrderTypes.BUY}` })
							.sort((item, prev) => { return parseInt(prev.orderId) - parseInt(item.orderId) })
							.map((item) => { return getOrderRow(item) })
					}
				</div>
			</div>
		)
	}

	const getTokenMedia = () => {
		if ( !currentChain ) { return null; }

		if ( token.tokenUrl?.startsWith('service:')  ) {

			const urlParams = token.tokenUrl.split(':');
			if ( urlParams[1].toLowerCase() === 'blocked' ) {
				return (
					<div className="inner">
						<div className="default">
							<img src={ icon_logo } alt="" />
							<span>Token url loading blocked</span>
						</div>
					</div>
				)
			}

			if ( urlParams[1].toLowerCase() === 'loading' ) {
				return (
					<a className="inner">
						<div className="default">
							<img src={ icon_logo } alt="" />
							<span>Loading NON-FUNGIBLE <br /> TOKEN Preview</span>
						</div>
					</a>
				)
			}

			if ( urlParams[1].toLowerCase() === 'cannotload' || urlParams[1].toLowerCase() === 'nourl' ) {
				return (
					<div className="inner">
						<div className="default">
							<img src={ icon_logo } alt="" />
							<span>Cannot load NON-FUNGIBLE <br /> TOKEN Preview</span>
						</div>
					</div>
				)
			}

		}

		if ( token.image?.startsWith('service:')  ) {

			const urlParams = token.image.split(':');
			if ( urlParams[1].toLowerCase() === 'blocked' ) {
				return (
					<div className="inner">
						<div className="default">
							<img src={ icon_logo } alt="" />
							<span>Image url loading blocked</span>
						</div>
					</div>
				)
			}

			if ( urlParams[1].toLowerCase() === 'loading' ) {
				return (
					<div className="inner">
						<div className="default">
							<img src={ icon_logo } alt="" />
							<span>Loading NON-FUNGIBLE <br /> TOKEN Preview</span>
						</div>
					</div>
				)
			}

			if ( urlParams[1].toLowerCase() === 'cannotload' || urlParams[1].toLowerCase() === 'nourl' ) {
				return (
					<div className="inner">
						<div className="default">
							<img src={ icon_logo } alt="" />
							<span>Cannot load NON-FUNGIBLE <br /> TOKEN Preview</span>
						</div>
					</div>
				)
			}
		}

		return (
			<div className="inner">
				<video className="img" src={ token.image } poster={ token.image } autoPlay={ true } muted={ true } loop={ true } />
			</div>
		)
	}


	const createAdvancedLoaderSell = (_currentChain: ChainType, _web3: Web3, _userAddress: string) => {
		const loaderStages: Array<AdvancedLoaderStageType> = [];

		if ( showOrderModal === 'sell' ) {
			loaderStages.push({
				id: 'approvetoken',
				sortOrder: 1,
				text: `Approving token to sell`,
				status: _AdvancedLoadingStatus.queued
			});
		}

		if ( showOrderModal === 'buy' ) {
			if ( inputOrderToken !== '0x0000000000000000000000000000000000000000' ) {
				console.log('push');
				loaderStages.push({
					id: 'approveerc20',
					sortOrder: 1,
					text: `Approving ${_currentChain.EIPPrefix}-20 price token`,
					status: _AdvancedLoadingStatus.queued,
				});
			}
		}

		loaderStages.push({
			id: 'placeorder',
			sortOrder: 2,
			text: `Placing order`,
			status: _AdvancedLoadingStatus.queued,
		});

		const advLoader = {
			title: 'Waiting to place order',
			stages: loaderStages
		};
		createAdvancedLoader(advLoader);
	}
	const approveOriginalToken = async (_currentChain: ChainType, _web3: Web3, _userAddress: string, isMultisig?: boolean) => {
		if ( !orderManagerAddress ) { return; }

		updateStepAdvancedLoader({
			id: 'approvetoken',
			status: _AdvancedLoadingStatus.loading,
		});

		const isApproved = await checkApprovalERC721(_currentChain.chainId, token.contractAddress, token.tokenId, _userAddress, orderManagerAddress);
		if ( !isApproved ) {
			try {
				if ( isMultisig ) {
					await setApprovalERC721Multisig(_web3, token.contractAddress, token.tokenId, _userAddress, orderManagerAddress)
				} else {
					await setApprovalERC721(_web3, token.contractAddress, token.tokenId, _userAddress, orderManagerAddress)
				}
			} catch(e: any) {
				setModal({
					type: _ModalTypes.error,
					title: `Cannot approve tokens to sell`,
					details: [
						`Contract address: ${token.contractAddress}`,
						`Token id: ${token.tokenId}`,
						`User address: ${_userAddress}`,
						`User address: ${orderManagerAddress}`,
						'',
						e.message || e,
					]
				});
				throw new Error();
			}
		}
		updateStepAdvancedLoader({
			id: 'approvetoken',
			status: _AdvancedLoadingStatus.complete,
		});
	}
	const approveerc20 = async (token: string, amount: BigNumber, _currentChain: ChainType, _web3: Web3, _userAddress: string, isMultisig?: boolean) => {

		if ( !orderManagerAddress ) { return; }

		let foundToken = erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === token.toLowerCase() });
		if ( !foundToken ) {
			foundToken = getNullERC20(token);
		}

		updateStepAdvancedLoader({
			id: 'approveerc20',
			status: _AdvancedLoadingStatus.loading,
			text: `Approving ${ foundToken.symbol }`,
		});

		if ( !amount ) { return; }

		const balance = await getERC20BalanceFromChain(_currentChain.chainId, token, _userAddress, orderManagerAddress);
		if ( balance.balance.lt(amount) ) {
			setModal({
				type: _ModalTypes.error,
				title: `Not enough ${foundToken.symbol}`,
				details: [
					`Token ${foundToken.symbol}: ${token}`,
					`User address: ${_userAddress}`,
					`User balance: ${tokenToFloat(balance.balance, foundToken.decimals).toString()}`,
				]
			});
			throw new Error();
		}
		if ( !balance.allowance || balance.allowance.amount.lt(amount) ) {
			try {
				if ( isMultisig ) {
					await makeERC20AllowanceMultisig(_web3, token, _userAddress, amount, orderManagerAddress);
				} else {
					await makeERC20Allowance(_web3, token, _userAddress, amount, orderManagerAddress);
				}
			} catch(e: any) {
				setModal({
					type: _ModalTypes.error,
					title: `Cannot approve ${foundToken.symbol}`,
					details: [
						`Token ${foundToken.symbol}: ${token}`,
						`User address: ${_userAddress}`,
						`User balance: ${tokenToFloat(balance.balance, foundToken.decimals).toString()}`,
						'',
						e.message || e,
					]
				});
				throw new Error();
			}
		}

		updateStepAdvancedLoader({
			id: 'approveerc20',
			text: `Approving ${_currentChain.EIPPrefix}-20 price tokens`,
			status: _AdvancedLoadingStatus.complete
		});

	}
	const sellSubmit = async (_currentChain: ChainType, _web3: Web3, _userAddress: string, isMultisig?: boolean) => {
		if ( !orderManagerAddress ) { return; }

		createAdvancedLoaderSell(_currentChain, _web3, _userAddress);

		try { await approveOriginalToken(_currentChain, _web3, _userAddress, isMultisig); } catch(ignored) { return; }

		updateStepAdvancedLoader({
			id: 'placeorder',
			status: _AdvancedLoadingStatus.loading
		});

		let foundToken = erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === inputOrderToken.toLowerCase() });
		if ( !foundToken ) {
			foundToken = getNullERC20(inputOrderToken);
		}
		const tokenForOrder: _AssetItem = {
			asset: {
				assetType: _AssetType.ERC721,
				contractAddress: token.contractAddress,
			},
			amount: '0',
			tokenId: token.tokenId,
		};
		const priceForOrder: NFTOrderPrice = {
			payAmount: tokenToInt(new BigNumber(inputOrderAmount), foundToken.decimals).toString(),
			payToken: inputOrderToken,
		};

		let txResp: any;
		try {
			if ( isMultisig ) {
				txResp = await makeNFTOrderSellMultisig(_web3, orderManagerAddress, tokenForOrder, priceForOrder);
			} else {
				txResp = await makeNFTOrderSell(_web3, orderManagerAddress, tokenForOrder, priceForOrder);
			}
		} catch(e: any) {
			setModal({
				type: _ModalTypes.error,
				title: `Cannot place order`,
				details: [
					`Token: ${token.contractAddress}, ${token.tokenId}`,
					`User address: ${_userAddress}`,
					`Order manager: ${orderManagerAddress}`,
					`Price: ${JSON.stringify(priceForOrder)}`,
					'',
					e.message || e
				]
			});
			return;
		}

		if ( isMultisig ) {
			setModal({
				type: _ModalTypes.success,
				title: `Transactions have been created`,
				buttons: [{
					text: 'Ok',
					clickFunc: () => {
						unsetModal();
						setShowOrderModal(undefined);
						fetchOrdersList(orderManagerAddress);
					}
				}],
				links: [{
					text: `View on ${_currentChain.explorerName}`,
					url: combineURLs(_currentChain.explorerBaseUrl, `/tx/${txResp.transactionHash}`)
				}],
			});
		} else {
			setModal({
				type: _ModalTypes.success,
				title: `Offer submitted`,
				buttons: [{
					text: 'Ok',
					clickFunc: () => {
						unsetModal();
						setShowOrderModal(undefined);
						fetchOrdersList(orderManagerAddress);
					}
				}],
				links: [{
					text: `View on ${_currentChain.explorerName}`,
					url: combineURLs(_currentChain.explorerBaseUrl, `/tx/${txResp.transactionHash}`)
				}],
			});
		}
	}
	const getModalSellBtn = () => {

		const submit = async () => {
			if ( !currentChain ) { return; }

			let _web3 = web3;
			let _userAddress = userAddress;
			let _currentChain = currentChain;

			if ( !web3 || !userAddress || await getChainId(web3) !== _currentChain.chainId ) {
				setLoading('Waiting for wallet');

				try {
					const web3Params = await getWeb3Force(_currentChain.chainId);

					_web3 = web3Params.web3;
					_userAddress = web3Params.userAddress;
					_currentChain = web3Params.chain;
					// unsetModal();
				} catch(e: any) {
					setModal({
						type: _ModalTypes.error,
						title: 'Error with wallet',
						details: [
							e.message || e
						]
					});
					return;
				}
			}

			if ( !_web3 || !_userAddress || !_currentChain ) { return; }
			const isMultisig = localStorageGet('authMethod').toLowerCase() === 'safe';
			sellSubmit(_currentChain, _web3, _userAddress, isMultisig);
		}

		const amount = tokenToFloat(new BigNumber(inputOrderAmount)).toString();
		if ( amount === 'NaN' ) {
			return (
				<button className="btn w-100" disabled={ true }>
					Sell
				</button>
			)
		}

		const foundToken = erc20List.find((item) => { return item.contractAddress.toLowerCase() === inputOrderToken.toLowerCase() });
		if ( !foundToken ) {
			return (
				<button
					className="btn w-100"
					onClick={ submit }
				>Sell for { amount } tokens</button>
			)
		}

		return (
			<button
				className="btn w-100"
				onClick={ submit }
			>Sell for { amount } { foundToken.symbol }</button>
		)
	}
	const offerSubmit = async (_currentChain: ChainType, _web3: Web3, _userAddress: string, isMultisig?: boolean) => {
		if ( !orderManagerAddress ) { return; }

		createAdvancedLoaderSell(_currentChain, _web3, _userAddress);

		let foundToken = erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === inputOrderToken.toLowerCase() });
		if ( !foundToken ) {
			foundToken = getNullERC20(inputOrderToken);
		}

		if ( inputOrderToken.toLowerCase() !== '0x0000000000000000000000000000000000000000' ) {
			try { await approveerc20(inputOrderToken, tokenToInt(new BigNumber(inputOrderAmount), foundToken.decimals), _currentChain, _web3, _userAddress, isMultisig); } catch(ignored) { return; }
		}

		updateStepAdvancedLoader({
			id: 'placeorder',
			status: _AdvancedLoadingStatus.loading
		});

		const tokenForOrder: _AssetItem = {
			asset: {
				assetType: _AssetType.ERC721,
				contractAddress: token.contractAddress,
			},
			amount: '0',
			tokenId: token.tokenId,
		};
		const priceForOrder: NFTOrderPrice = {
			payAmount: tokenToInt(new BigNumber(inputOrderAmount), foundToken.decimals).toString(),
			payToken: inputOrderToken,
		};

		let txResp: any;
		try {
			if ( showOrderModal === 'sell' ) {
				if ( isMultisig ) {
					txResp = await makeNFTOrderSellMultisig(_web3, orderManagerAddress, tokenForOrder, priceForOrder);
				} else {
					txResp = await makeNFTOrderSell(_web3, orderManagerAddress, tokenForOrder, priceForOrder);
				}
			} else {
				if ( isMultisig ) {
					txResp = await makeNFTOrderBuyMultisig(_web3, orderManagerAddress, tokenForOrder, priceForOrder);
				} else {
					txResp = await makeNFTOrderBuy(_web3, orderManagerAddress, tokenForOrder, priceForOrder);
				}
			}
		} catch(e: any) {
			setModal({
				type: _ModalTypes.error,
				title: `Cannot place order`,
				details: [
					`Order type: ${showOrderModal}`,
					`Token: ${token.contractAddress}, ${token.tokenId}`,
					`User address: ${_userAddress}`,
					`Order manager: ${orderManagerAddress}`,
					`Price: ${JSON.stringify(priceForOrder)}`,
					'',
					e.message || e
				]
			});
			return;
		}

		if ( isMultisig ) {
			setModal({
				type: _ModalTypes.success,
				title: `Transactions have been created`,
				buttons: [{
					text: 'Ok',
					clickFunc: () => {
						unsetModal();
						setShowOrderModal(undefined);
						fetchOrdersList(orderManagerAddress);
					}
				}],
				links: [{
					text: `View on ${_currentChain.explorerName}`,
					url: combineURLs(_currentChain.explorerBaseUrl, `/tx/${txResp.transactionHash}`)
				}],
			});
		} else {
			setModal({
				type: _ModalTypes.success,
				title: `Offer submitted`,
				buttons: [{
					text: 'Ok',
					clickFunc: () => {
						unsetModal();
						setShowOrderModal(undefined);
						fetchOrdersList(orderManagerAddress);
					}
				}],
				links: [{
					text: `View on ${_currentChain.explorerName}`,
					url: combineURLs(_currentChain.explorerBaseUrl, `/tx/${txResp.transactionHash}`)
				}],
			});
		}
	}
	const getModalBuyBtn = () => {

		const submit = async () => {
			if ( !currentChain ) { return; }

			let _web3 = web3;
			let _userAddress = userAddress;
			let _currentChain = currentChain;

			if ( !web3 || !userAddress || await getChainId(web3) !== _currentChain.chainId ) {
				setLoading('Waiting for wallet');

				try {
					const web3Params = await getWeb3Force(_currentChain.chainId);

					_web3 = web3Params.web3;
					_userAddress = web3Params.userAddress;
					_currentChain = web3Params.chain;
					// unsetModal();
				} catch(e: any) {
					setModal({
						type: _ModalTypes.error,
						title: 'Error with wallet',
						details: [
							e.message || e
						]
					});
					return;
				}
			}

			if ( !_web3 || !_userAddress || !_currentChain ) { return; }
			const isMultisig = localStorageGet('authMethod').toLowerCase() === 'safe';
			offerSubmit(_currentChain, _web3, _userAddress, isMultisig);
		}

		const amount = tokenToFloat(new BigNumber(inputOrderAmount)).toString();
		if ( amount === 'NaN' ) {
			return (
				<button className="btn w-100" disabled={ true }>
					Offer
				</button>
			)
		}

		const foundToken = erc20List.find((item) => { return item.contractAddress.toLowerCase() === inputOrderToken.toLowerCase() });
		if ( !foundToken ) {
			return (
				<button
					className="btn w-100"
					onClick={ submit }
				>Offer { amount } tokens</button>
			)
		}

		return (
			<button
				className="btn w-100"
				onClick={ submit }
			>Offer { amount } { foundToken.symbol }</button>
		)
	}
	const getOrderModal = () => {
		if ( !showOrderModal ) { return null; }
		if ( !currentChain ) { return null; }

		const priceTokensList = [
			...erc20List,
		];
		// if ( showOrderModal === 'sell' ) {
		// 	priceTokensList.push(chainTypeToERC20(currentChain))
		// }

		return (
			<div className="modal">
			<div
				className="modal__inner"
				onClick={(e) => {
					e.stopPropagation();
					if ((e.target as HTMLTextAreaElement).className === 'modal__inner') {
						setInputOrderToken('');
						setInputOrderAmount('');
						setShowOrderModal(undefined);
					}
				}}
			>
				<div className="modal__bg"></div>
				<div className="container">
				<div className="modal__content">
				<div
					className="modal__close"
					onClick={() => {
						setInputOrderToken('');
						setInputOrderAmount('');
						setShowOrderModal(undefined);
					}}
				>
					<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
						<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
					</svg>
				</div>
				<div className="c-add">
					<div className="c-add__text">
						{
							showOrderModal === 'sell' ? (
								<div className="h2">Set your price</div>
							) : (
								<div className="h2">Offer your price</div>
							)
						}
					</div>
					<div className="c-add__coins">
						<div className="c-add__form">
							<div className="form-row">
								<div className="col-12">
									<div className="mb-4">
										<div className="select-group">
											<CoinSelector
												tokens        = { priceTokensList }
												selectedToken = { inputOrderToken }
												onChange      = {(address: string) => {
													setInputOrderToken(address);
												}}
											/>
											<input
												className="input-control"
												type="text"
												placeholder="Enter or select price token"
												value={ inputOrderToken }
												onChange={(e) => {
													const value = e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, "");
													if ( Web3.utils.isAddress(value) ) {
														const foundToken = erc20List.find((item: ERC20Type) => { return item.contractAddress.toLowerCase() === value.toLowerCase() });
														if ( !foundToken ) {
															requestERC20Token(value, userAddress);
														}
													}
													setInputOrderToken(value);
												}}
											/>
										</div>
									</div>
								</div>
								<div className="col-12 col-md-6">
									<div className="mb-4">
									<input
										className="input-control"
										type="text"
										placeholder="0.00"
										value={ addThousandSeparator(inputOrderAmount) }
										onChange={(e) => {
											let value = removeThousandSeparator(e.target.value);
											if ( value !== '' && !value.endsWith('.') && !value.endsWith('0') ) {
												if ( new BigNumber(value).isNaN() ) { return; }
												value = new BigNumber(value).toString();
											}
											setInputOrderAmount(value);
										}}
									/>
									</div>
								</div>
								<div className="col-12 mt-4">
									{
										showOrderModal === 'sell' ?
										getModalSellBtn() :
										getModalBuyBtn()
									}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
			</div>
			</div>
		)
	}
	const getSellBtn = () => {

		if ( !orderManagerAddress ) { return null; }

		if ( !userAddress ) { return null; }
		if ( props.token.assetType === _AssetType.ERC1155 ) { return null; }

		if ( !props.token.owner ) { return null; }
		if ( props.token.owner.toLowerCase() !== userAddress.toLowerCase() ) { return null; }

		const sellOrder = orderList.find((item) => { return `${item.orderType}` === `${NFTOrderTypes.SELL}` });
		let sellOrderFoundToken: ERC20Type = getNullERC20(inputOrderToken);
		let sellOrderAmount: string = '0';

		if ( sellOrder ) {
			const foundToken = erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === sellOrder.price.payToken.toLowerCase() });
			if ( foundToken ) {
				sellOrderFoundToken = foundToken;
			} else {
				requestERC20Token(sellOrder.price.payToken, userAddress);
			}
			sellOrderAmount = tokenToFloat(new BigNumber(sellOrder.price.payAmount), sellOrderFoundToken.decimals).toString();
		}

		return (
			<>
			<div className="mb-4 mt-4">
				<button
					className="btn btn-yellow"
					onClick={async () => {
						setShowOrderModal('sell')
					}}
				>{ sellOrder ? 'Set new price' : 'Sell' }</button>
			</div>
			{
				sellOrder ? (
					<div className="container status mt-4">
						<div
							className="modal__close"
							style={{
								top: '0',
								right: '0',
							}}
							onClick={async () => {
								if ( !currentChain ) { return; }

								let _web3 = web3;
								let _userAddress = userAddress;
								let _currentChain = currentChain;

								if ( !web3 || !userAddress || await getChainId(web3) !== _currentChain.chainId ) {
									setLoading('Waiting for wallet');

									try {
										const web3Params = await getWeb3Force(_currentChain.chainId);

										_web3 = web3Params.web3;
										_userAddress = web3Params.userAddress;
										_currentChain = web3Params.chain;
										// unsetModal();
									} catch(e: any) {
										setModal({
											type: _ModalTypes.error,
											title: 'Error with wallet',
											details: [
												e.message || e
											]
										});
										return;
									}
								}

								if ( !_web3 || !_userAddress || !_currentChain ) { return; }
								const isMultisig = localStorageGet('authMethod').toLowerCase() === 'safe';
								removeOrderSubmit(sellOrder.orderId, _currentChain, _web3, _userAddress, isMultisig);
							}}
						>
							<img src={ icon_i_del } />
						</div>

						<p>Your price </p>
						<p><b>{ sellOrderAmount } { sellOrderFoundToken.symbol }</b></p>
					</div>
				) : null
			}
			</>
		)
	}

	const createAdvancedLoaderTake = (order: NFTOrder, _currentChain: ChainType, _web3: Web3, _userAddress: string) => {
		const loaderStages: Array<AdvancedLoaderStageType> = [];

		if ( decodeNFTOrderType(order.orderType) === NFTOrderTypes.SELL ) {
			if ( order.price.payToken !== '0x0000000000000000000000000000000000000000' ) {
				loaderStages.push({
					id: 'approveerc20',
					sortOrder: 1,
					text: `Approving ${_currentChain.EIPPrefix}-20 price token`,
					status: _AdvancedLoadingStatus.queued,
				});
			}
		} else {
			loaderStages.push({
				id: 'approvetoken',
				sortOrder: 1,
				text: `Approving token to sell`,
				status: _AdvancedLoadingStatus.queued
			});
		}

		loaderStages.push({
			id: 'takeorder',
			sortOrder: 2,
			text: `Taking order`,
			status: _AdvancedLoadingStatus.queued,
		});

		const advLoader = {
			title: 'Waiting to take order',
			stages: loaderStages
		};
		createAdvancedLoader(advLoader);
	}
	const takeOrderSubmit = async (order: NFTOrder, _currentChain: ChainType, _web3: Web3, _userAddress: string, isMultisig?: boolean) => {
		if ( !orderManagerAddress ) { return; }

		createAdvancedLoaderTake(order, _currentChain, _web3, _userAddress);

		if ( decodeNFTOrderType(order.orderType) === NFTOrderTypes.SELL ) {
			if ( order.price.payToken.toLowerCase() !== '0x0000000000000000000000000000000000000000' ) {
				try { await approveerc20(order.price.payToken, new BigNumber(order.price.payAmount), _currentChain, _web3, _userAddress, isMultisig); } catch(ignored) { return; }
			}
		} else {
			try { await approveOriginalToken(_currentChain, _web3, _userAddress, isMultisig); } catch(ignored) { return; }
		}

		updateStepAdvancedLoader({
			id: 'takeorder',
			status: _AdvancedLoadingStatus.loading
		});

		let foundToken = erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === inputOrderToken.toLowerCase() });
		if ( !foundToken ) {
			foundToken = getNullERC20(inputOrderToken);
		}
		const tokenForOrder: _AssetItem = {
			asset: {
				assetType: _AssetType.ERC721,
				contractAddress: token.contractAddress,
			},
			amount: '0',
			tokenId: token.tokenId,
		};

		let txResp: any;
		try {
			if ( isMultisig ) {
				txResp = await takeNFTOrderMultisig(_web3, orderManagerAddress, order, tokenForOrder);
			} else {
				txResp = await takeNFTOrder(_web3, orderManagerAddress, order, tokenForOrder);
			}
		} catch(e: any) {
			setModal({
				type: _ModalTypes.error,
				title: `Cannot take order`,
				details: [
					`Token: ${token.contractAddress}, ${token.tokenId}`,
					`User address: ${_userAddress}`,
					`Order manager: ${orderManagerAddress}`,
					`Order: ${JSON.stringify(order)}`,
					'',
					e.message || e
				]
			});
			return;
		}

		if ( isMultisig ) {
			setModal({
				type: _ModalTypes.success,
				title: `Transactions have been created`,
				buttons: [{
					text: 'Ok',
					clickFunc: () => {
						window.location.reload();
					}
				}],
				links: [{
					text: `View on ${_currentChain.explorerName}`,
					url: combineURLs(_currentChain.explorerBaseUrl, `/tx/${txResp.transactionHash}`)
				}],
			});
		} else {
			setModal({
				type: _ModalTypes.success,
				title: decodeNFTOrderType(order.orderType) === NFTOrderTypes.SELL ? `Token successfully bought` : `Token successfully sold`,
				text: currentChain?.hasOracle ? [
					{ text: 'After oracle synchronization, your changes will appear on dashboard soon. Please refresh the dashboard page' },
				] : [],
				buttons: [{
					text: 'Ok',
					clickFunc: () => {
						window.location.reload();
					}
				}],
				links: [{
					text: `View on ${_currentChain.explorerName}`,
					url: combineURLs(_currentChain.explorerBaseUrl, `/tx/${txResp.transactionHash}`)
				}],
			});
		}
	}
	const getBuyBtn = () => {

		if ( !orderManagerAddress ) { return null; }

		if ( props.token.assetType === _AssetType.ERC1155 ) { return null; }

		if ( !props.token.owner ) { return null; }
		if ( userAddress && props.token.owner.toLowerCase() === userAddress.toLowerCase() ) { return null; }
		if ( isOwnerContract ) { return null; }


		const sellOrder = orderList.find((item) => { return `${item.orderType}` === `${NFTOrderTypes.SELL}` });
		let sellOrderFoundToken: ERC20Type = getNullERC20(inputOrderToken);
		let sellOrderAmount: string = '0';

		if ( sellOrder ) {
			const foundToken = erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === sellOrder.price.payToken.toLowerCase() });
			if ( foundToken ) {
				sellOrderFoundToken = foundToken;
			} else {
				requestERC20Token(sellOrder.price.payToken, userAddress);
			}
			sellOrderAmount = tokenToFloat(new BigNumber(sellOrder.price.payAmount), sellOrderFoundToken.decimals).toString();
		}

		let buyOrder: NFTOrder | undefined = undefined;
		let buyOrderFoundToken: ERC20Type = getNullERC20(inputOrderToken);
		let buyOrderAmount: string = '0';
		if ( userAddress ) {
			buyOrder = orderList.find((item) => {
				return `${item.orderType}` === `${NFTOrderTypes.BUY}` &&
					item.orderMaker.toLowerCase() === userAddress.toLowerCase()
			});
			if ( buyOrder ) {
				const foundToken = erc20List.find((item) => { return item.contractAddress.toLowerCase() === buyOrder?.price.payToken.toLowerCase() });
				if ( foundToken ) { buyOrderFoundToken = foundToken; }
				buyOrderAmount = tokenToFloat(new BigNumber(buyOrder.price.payAmount), foundToken?.decimals).toString();
			}
		}

		return (
			<div className="wnft-card__unwrap">
			{
				sellOrder ? (
					<div className="mb-4">
						<button
							className="btn btn-grad"
							onClick={async () => {
								if ( !currentChain ) { return; }

								let _web3 = web3;
								let _userAddress = userAddress;
								let _currentChain = currentChain;

								if ( !web3 || !userAddress || await getChainId(web3) !== _currentChain.chainId ) {
									setLoading('Waiting for wallet');

									try {
										const web3Params = await getWeb3Force(_currentChain.chainId);

										_web3 = web3Params.web3;
										_userAddress = web3Params.userAddress;
										_currentChain = web3Params.chain;
										// unsetModal();
									} catch(e: any) {
										setModal({
											type: _ModalTypes.error,
											title: 'Error with wallet',
											details: [
												e.message || e
											]
										});
										return;
									}
								}

								if ( !_web3 || !_userAddress || !_currentChain ) { return; }
								const isMultisig = localStorageGet('authMethod').toLowerCase() === 'safe';
								takeOrderSubmit(sellOrder, _currentChain, _web3, _userAddress, isMultisig);
							}}
						>Buy for { sellOrderAmount } { sellOrderFoundToken.symbol } </button>
					</div>
				) : null
			}
			<div>
				<button
					className="btn btn-outline"
					onClick={() => {
						setShowOrderModal('buy');
					}}
				>Make offer</button>
			</div>
			{
				buyOrder ? (
					<div className="status mt-4">
						<p>Your offer </p>
						<p><b>{ buyOrderAmount } { buyOrderFoundToken.symbol }</b></p>
					</div>
				) : null
			}
			</div>
		)
	}

	const getWrapBtn = () => {

		if ( !currentChain ) { return null; }
		if ( !userAddress  ) { return null; }

		if (
			( token.owner && token.owner.toLowerCase() === userAddress.toLowerCase() ) ||
			( token.amount && token.amount.gt(0) )
		) {
			return (
				<div className="wnft-card__unwrap">
					<div className="">
						<a
							href={`/wrap/${currentChain.chainId}/${token.contractAddress}/${token.tokenId}`}
							className="btn btn-grad"
						>
							Wrap
						</a>
					</div>

					{ getSellBtn() }
				</div>
			)
		}
	}

	const getOwnerBlock = () => {
		if ( !currentChain ) { return; }
		if (
			(
				token.assetType === _AssetType.ERC1155 || !token.owner
			) &&
			!token.owner &&
			!token.blockNumber
		) { return null; }

		return (
			<div className="c-wrap p-0">
				<div
					className={`c-wrap__toggle ${ ownerBlockOpened ? 'active' : '' }`}
					onClick={() => { setOwnerBlockOpened(!ownerBlockOpened) }}
				>
					<div><b>NFT info</b></div>
				</div>
				<div className="c-wrap__dropdown border-top pt-2 pt-md-3">
					{
						token.assetType !== _AssetType.ERC1155 && token.owner ? (
							<div className="flex-nowrap row py-3">
								<div className="col-4">Owner</div>
								<TippyWrapper msg={ token.owner }>
									<div className="col-8 text-right">
										{ compactString(token.owner) }
										{ ' ' }
										<CopyToClipboard
											text={ token.owner }
											onCopy={() => {
												if ( !token ) { return; }
												setCopiedHintWhere('copyowner');
												clearTimeout(copiedHintTimer);
												copiedHintTimer = window.setTimeout(() => { setCopiedHintWhere(''); }, copiedHintTimeout*1000);
											}}
										>
											<button className="btn-copy">
												<img src={ icon_i_copy } alt="" />
												{ getCopiedHint('copyowner') }
											</button>
										</CopyToClipboard>
									</div>
								</TippyWrapper>
							</div>
						) : null
					}
					{
						blockDate ? (
							<div className="flex-nowrap row py-3">
								<div className="col-4">Date/block</div>
								<div className="col-8 text-right">
									<a target="_blank" rel="noopener noreferrer" href={ combineURLs(currentChain.explorerBaseUrl, `/block/${token.blockNumber}`) }>
										{ dateToStrWithMonth(unixtimeToDate(blockDate)) }
									</a>
								</div>
							</div>
						) :
						token.blockNumber ? (
							<div className="flex-nowrap row py-3">
								<div className="col-4">Date/block</div>
								<div className="col-8 text-right">
									<a target="_blank" rel="noopener noreferrer" href={ combineURLs(currentChain.explorerBaseUrl, `/block/${token.blockNumber}`) }>
										{ token.blockNumber }
									</a>
								</div>
							</div>
						) : null
					}

				</div>
			</div>
		)
	}

	const getDescription = () => {
		if ( !token.description ) { return null; }

		if ( token.description.length < DESCRIPTION_LENGTH ) {
			return (
				<div className="desc">
					<p>{ token.description }</p>
				</div>
			)
		}

		if ( !descriptionOpened ) {
			return (
				<div className="desc">
					<p>
						{ token.description.slice(0, DESCRIPTION_LENGTH) }
						<button
							className="btn-more"
							onClick={() => { setDescriptionOpened(!descriptionOpened) }}
						>
							...more
						</button>
					</p>
				</div>
			)
		} else {
			return (
				<div className="desc">
					<p>
						{ token.description }
						<button
							className="btn-more"
							onClick={() => { setDescriptionOpened(!descriptionOpened) }}
						>
							...less
						</button>
					</p>
				</div>
			)
		}
	}
	const getNFTInfo = () => {
		return (
			<div className="wnft-header__info">
				<div className="h4">NFT Info</div>
				<div className="desc">
					<div className="title">{ token.name }</div>
					{ getDescription() }
				</div>
				<div className="row align-items-center mt-4">
					<div className="col-12 col-sm mb-3 mb-sm-0">
						<ul className="meta">
							{
								token.amount && token.amount.gt(0) ? (
									<li><span className="text-muted">Copies:</span> <b>{ token.amount.toString() }</b></li>
								) : null
							}
							{
								token.totalSupply && token.totalSupply.gt(0) ? (
									<li><span className="text-muted">Total supply:</span> <b>{ token.totalSupply.toString() }</b></li>
								) : null
							}
						</ul>
					</div>
					<div className="col-12 col-sm-auto">
						<ExternalApps
							chainId={ token.chainId }
							contractAddress={ token.contractAddress }
							tokenId={ token.tokenId }
							userAddress={ userAddress }
						/>
					</div>
				</div>
			</div>
		)
	}

	const getTokenUrl = () => {
		if ( !token ) { return ''; }
		if ( token.tokenUrlRaw ) { return `Token URI: ${token.tokenUrlRaw}`; }
		if ( token.tokenUrl ) { return `Token URI: ${token.tokenUrl}`; }
	}
	const getTokenUrlJson = () => {

		let bodyToRender = 'Cannot show JSON';
		try {
			bodyToRender = JSON.stringify(token.tokenUrlBody, null, 4)
		} catch(ignored) {}

		return (
			<div className="c-wrap">
				<div className="w-card__json">
					<div className="row align-items-center">
						<div className="col-12 col-sm mb-3 mb-sm-0">
							<CopyToClipboard
								text={ token.tokenUrlRaw || '' }
								onCopy={() => {
									if ( !token ) { return; }
									setCopiedHintWhere('copyjson');
									clearTimeout(copiedHintTimer);
									copiedHintTimer = window.setTimeout(() => { setCopiedHintWhere(''); }, copiedHintTimeout*1000);
								}}
							>
								<button className="btn-copy">
									<span><b>Token URI</b></span>
									<img src={ icon_i_copy } alt="" />
									{ getCopiedHint('copyjson') }
								</button>
							</CopyToClipboard>
						</div>
						<div className="col-12 col-sm-auto">
							<div className="btn-group">
								<a
									className="btn btn-sm btn-gray"
									target="_blank" rel="noopener noreferrer"
									href={ token.tokenUrl }
								>
									<span className="mr-2">Download JSON</span>
									<img src={ icon_i_download } alt="" />
								</a>
							</div>
						</div>
					</div>
					<div className="json-code">
						<div className="scroll-dark-bg">
							<pre>
								{ getTokenUrl() }
								{ '\n\n' }
								{ bodyToRender }</pre>
						</div>
					</div>
				</div>
			</div>
		)
	}

	return (
		<>
			<main className="s-main">
			<div className="container">

			{ getBreadcrumbs() }

			<div className="wnft-content">
				<div className="row">
					<div className="col-lg-8 d-lg-none">
						{ getHeader() }
					</div>
					<div className="col-lg-4">
						<div className="wnft-card">

							{ getTokenMedia() }
							{ getWrapBtn() }

							{ getBuyBtn() }

						</div>
						{ getOwnerBlock() }
					</div>
					<div className="col-lg-8">

						<div className="d-none d-lg-block">
							{ getHeader() }
						</div>

						{ getNFTInfo() }

						<OwnerSocials
							chainId={ token.chainId }
							contractAddress={ token.contractAddress }
							tokenId={ token.tokenId }
							userAddress={ token.owner }
						/>

						{ getOrdersBlock() }

						{ getTokenUrlJson() }

					</div>
				</div>

				<TokenHistory token={token} />

			</div>
			</div>
			</main>

		{
			transferPopup ?
			(
				<TransferPopup
					token={ props.token }
					closePopup={()=>{ setTransferPopup(false) }}
				/>
			)
			: null
		}
		{
			approvePopup ?
			(
				<ApprovePopup
					token={ token }
					closePopup={()=>{ setApprovePopup(false) }}
				/>
			)
			: null
		}
		{ getOrderModal() }
		</>
	)
}