
import { useContext } from "react";
import {
	CollateralItem,
	ERC20Type,
	WNFTsStat,
	chainTypeToERC20,
	getNullERC20,
	tokenToFloat,
} from "@envelop/envelop-client-core";
import {
	ERC20Context,
	Web3Context
} from "../../dispatchers";

type CollateralStatProps = {
	collateralStat: WNFTsStat,
}

export default function CollateralStat(props: CollateralStatProps) {

	const {
		collateralStat,
	} = props;

	const {
		currentChain,
	} = useContext(Web3Context);
	const {
		erc20List,
	} = useContext(ERC20Context);

	const getCollateralStatItem = (item: CollateralItem) => {
		if ( !item.amount ) { return null; }
		if ( !currentChain ) { return null; }

		let foundToken: ERC20Type | undefined;
		if ( item.contractAddress === '0x0000000000000000000000000000000000000000' ) {
			foundToken = chainTypeToERC20(currentChain);
		} else {
			foundToken = erc20List.find((iitem) => { return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() });
		}
		if ( !foundToken ) {
			foundToken = getNullERC20(item.contractAddress)
		}

		return (
			<div className="item" key={ `${item.contractAddress}${item.tokenId}` }>
				<div className="row">
					<div className="col-12 col-sm-3 col-md-2 mb-2">
						<div className="tb-coin">
							<span className="i-coin">
								<img src={ foundToken.icon } alt="" />
							</span>
							<span className="name">{ foundToken.symbol }</span>
						</div>
					</div>
					<div className="col-12 col-sm mb-2"><span className="text-break">{ tokenToFloat(item.amount, foundToken.decimals).toString() }</span></div>
					{/* <div className="col-12 col-sm-3 mb-2 text-right"><span className="text-muted">~ 100 DAI</span></div> */}
				</div>
			</div>
		)
	}

	return (
		<div className="db-section">
			<div className="container">
				<div className="c-wrap__table light">
					<div className="item item-header">
						<div className="row">
						<div className="mb-2 col-md-2 text-muted">Token</div>
						<div className="mb-2 col-md text-muted">Amount</div>
						<div className="mb-2 col-md-3 align-right"></div>
						</div>
					</div>
					{
						collateralStat.collaterals
							.sort((item, prev) => { return item.contractAddress.localeCompare(prev.contractAddress) })
							.map((item) => { return getCollateralStatItem(item) })
					}
				</div>
			</div>
		</div>
	)
}