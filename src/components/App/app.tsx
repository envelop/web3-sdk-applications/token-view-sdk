
import {
	useContext,
	useEffect,
	useRef,
	useState
} from 'react';
import {
	useMatch,
	useNavigate
} from 'react-router-dom';
import {
	BigNumber,
	NFT,
	WNFT,
	getABI,
	getNFTById,
	getWNFTById,
	isContractWNFTFromChain
} from '@envelop/envelop-client-core';

import {
	InfoModalContext,
	Web3Context,
	_ModalTypes
} from '../../dispatchers';

import Header       from '../Header';
import Footer       from '../Footer';
import WNFTViewPage from '../WNFTViewPage';
import NFTViewPage  from '../NFTViewPage';

import icon_loading from "../../static/pics/loading.svg"

import config       from '../../app.config.json';
import TippyWrapper from '../TippyWrapper';
import InfoMessages from '../InfoMessages';


export const tokenToIntPretty = (num: BigNumber) => {

	const DECIMALS_TO_SHOW = 3;

	if ( num.lt(10**-DECIMALS_TO_SHOW) ) {
		return (<TippyWrapper msg={ num.toString() }><>&lt;{10**-DECIMALS_TO_SHOW}</></TippyWrapper>)
	}
	return (<>{num.toFixed(DECIMALS_TO_SHOW)}</>)

}

export default function App() {

	const [ WNFT, setWNFT ] = useState<WNFT | undefined>(undefined);
	const [ NFT,  setNFT  ] = useState<NFT | undefined>(undefined);

	const tokenLoadStarted = useRef(false);

	const {
		setModal,
	} = useContext(InfoModalContext);
	const {
		web3,
		userAddress
	} = useContext(Web3Context);

	const match = useMatch(':chainId/:contractAddress/:tokenId');

	const getToken = async (chainId: number, contractAddress: string, tokenId: string) => {

		if ( web3 === undefined ) { return; }
		if ( web3 !== null && userAddress === undefined ) { return; }
		if ( tokenLoadStarted.current ) { return; }

		tokenLoadStarted.current = true;

		const isWNFT = await isContractWNFTFromChain(chainId, contractAddress, tokenId);
		try {
			if ( isWNFT ) {
				const token = await getWNFTById(chainId, contractAddress, tokenId, userAddress)
				setWNFT(token);
			} else {
				const token = await getNFTById(chainId, contractAddress, tokenId, userAddress)
				setNFT(token);
			}
		} catch(e: any) {
			setModal({
				type: _ModalTypes.error,
				title: 'Cannot load token',
				details: [
					`chainId: ${chainId}`,
					`contractAddress: ${contractAddress}`,
					`tokenId: ${tokenId}`,
					'',
					e.message || e
				]
			})
		}

		tokenLoadStarted.current = false;

	}
	useEffect(() => {

		if ( web3 === undefined ) { return; }
		if ( tokenLoadStarted.current ) { return; }

		if ( !match || !match.params.chainId || !match.params.contractAddress || !match.params.tokenId ) {
			setModal({
				type: _ModalTypes.error,
				title: 'Wrong url format',
				buttons: [{
					text: 'Ok',
					clickFunc: () => {
						window.location.href = '/dashboard';
					}
				}],
			});
			return;
		}

		const chainId = parseInt(match.params.chainId);
		const {
			contractAddress,
			tokenId
		} = match.params;

		const foundChainData = config.CHAIN_SPECIFIC_DATA.find((item) => { return item.chainId === chainId });
		if ( !foundChainData ) {
			setModal({
				type: _ModalTypes.error,
				title: 'Unsupported chain',
				buttons: [{
					text: 'Ok',
					clickFunc: () => {
						window.location.href = '/dashboard';
					}
				}],
			});
			return
		}
		getToken(chainId, contractAddress, tokenId);

	}, [ web3, match, userAddress ])

	return (
		<>
			<InfoMessages />
			<Header />
			{ WNFT ? ( <WNFTViewPage token={WNFT} /> ) : null }
			{  NFT ? ( <NFTViewPage  token={NFT}  /> ) : null }
			{
				!WNFT && !NFT ? (
					<main className="s-main">
					<div className="mt-4">
					<div className="container">
					<div className="lp-list">
					<div className="lp-list__footer">
						<img className="loading" src={ icon_loading } alt="" />
					</div>
					</div>
					</div>
					</div>
					</main>
				) : null
			}
			<Footer />
		</>
	)

}