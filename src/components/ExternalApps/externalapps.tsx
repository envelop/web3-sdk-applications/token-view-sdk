
import { useMemo } from 'react';
import config from '../../app.config.json';
import default_coin   from '../../static/pics/coins/_default.svg';
import TippyWrapper from '../TippyWrapper';

type _ExternalApp = {
	name: string,
	urlTemplate: string,
	urlFallback?: string,
}
type ExternalApp = {
	name: string,
	url: string,
	icon: string,
	iconHover: string,
}

type ExternalAppsProps = {
	chainId: number,
	contractAddress: string,
	tokenId: string,
	userAddress?: string,
}

export default function ExternalApps(props: ExternalAppsProps) {

	const {
		chainId,
		contractAddress,
		tokenId,
		userAddress
	} = props;

	const getAppIcon = (name: string) => {
		let appIcon = default_coin;
		try { appIcon = require(`../../static/pics/links/${name.toLowerCase()}-logo.jpeg`) } catch (ignored) {}
		try { appIcon = require(`../../static/pics/links/${name.toLowerCase()}-logo.jpg` ) } catch (ignored) {}
		try { appIcon = require(`../../static/pics/links/${name.toLowerCase()}-logo.png` ) } catch (ignored) {}
		try { appIcon = require(`../../static/pics/links/${name.toLowerCase()}-logo.svg` ) } catch (ignored) {}

		let appIconHover = appIcon;
		try { appIconHover = require(`../../static/pics/links/${name.toLowerCase()}-logo-gray.jpeg`) } catch (ignored) {}
		try { appIconHover = require(`../../static/pics/links/${name.toLowerCase()}-logo-gray.jpg` ) } catch (ignored) {}
		try { appIconHover = require(`../../static/pics/links/${name.toLowerCase()}-logo-gray.png` ) } catch (ignored) {}
		try { appIconHover = require(`../../static/pics/links/${name.toLowerCase()}-logo-gray.svg` ) } catch (ignored) {}

		return { icon: appIcon, iconHover: appIconHover }
	}

	const apps: Array<ExternalApp> = useMemo(() => {

		const processUrl = (item: _ExternalApp) => {

			if ( item.urlTemplate.includes('${userAddress}') ) {
				if ( userAddress !== undefined ) {
					return item.urlTemplate
						.replaceAll('${contractAddress}', contractAddress)
						.replaceAll('${tokenId}', tokenId)
						.replaceAll('${userAddress}', userAddress)
				}
				if ( item.urlFallback !== undefined ) {
					return item.urlFallback
						.replaceAll('${contractAddress}', contractAddress)
						.replaceAll('${tokenId}', tokenId)
				}

				return item.urlTemplate
					.replaceAll('${contractAddress}', contractAddress)
					.replaceAll('${tokenId}', tokenId)

			}

			return item.urlTemplate
				.replaceAll('${contractAddress}', contractAddress)
				.replaceAll('${tokenId}', tokenId)
		}

		const foundChain = config.CHAIN_SPECIFIC_DATA.find((item) => { return item.chainId === chainId });
		if ( !foundChain || !foundChain.externalLinks ) { return [] }

		return foundChain.externalLinks.map((item) => {
			return {
				...item,
				url: processUrl(item),
				...getAppIcon(item.name)
			}
		});

	}, [ chainId ])

	const getItem = (item: ExternalApp) => {
		return (
			<TippyWrapper msg={ item.name } key={item.name}>
			<li>
				<a href={ item.url } target="_blank" rel="nofollow noopener noreferrer">
					<img src={ item.iconHover } alt="" />
					<img className="hover" src={ item.icon } alt="" />
				</a>
			</li>
			</TippyWrapper>
		)
	}

	const getBody = () => {
		if ( !apps.length ) { return null; }

		return (
			<ul className="wnft-exlinks">
				{ apps.map((item) => { return getItem(item) }) }
			</ul>
		)

	}

	return getBody();

}