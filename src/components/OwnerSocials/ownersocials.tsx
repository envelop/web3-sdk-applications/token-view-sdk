
import { useEffect, useMemo, useState } from 'react';
import config from '../../app.config.json';
import default_coin   from '../../static/pics/coins/_default.svg';
import { getDefaultWeb3 } from '@envelop/envelop-client-core';

type _ExternalApp = {
	name: string,
	urlTemplate: string,
	urlFallback?: string,
}
type ExternalApp = {
	name: string,
	url: string,
	icon: string,
	iconHover: string,
}

type OwnerSocialsProps = {
	chainId: number,
	contractAddress: string,
	tokenId: string,
	userAddress?: string,
}

export default function OwnerSocials(props: OwnerSocialsProps) {

	const {
		chainId,
		contractAddress,
		tokenId,
		userAddress
	} = props;

	const [ isOwnerContract, setIsOwnerContract ] = useState(true);

	useEffect(() => {
		setIsOwnerContract(true);

		getDefaultWeb3(chainId).then(async (data) => {
			if ( !userAddress ) { return; }
			if ( data ) {
				const code = await data.eth.getCode(userAddress);
				if ( code.toLowerCase() === '0x' ) { setIsOwnerContract(false); }
			}
		})
	}, [ chainId, contractAddress, tokenId, userAddress ])

	const getAppIcon = (name: string) => {
		let appIcon = default_coin;
		try { appIcon = require(`../../static/pics/links/${name.toLowerCase()}-logo.jpeg`) } catch (ignored) {}
		try { appIcon = require(`../../static/pics/links/${name.toLowerCase()}-logo.jpg` ) } catch (ignored) {}
		try { appIcon = require(`../../static/pics/links/${name.toLowerCase()}-logo.png` ) } catch (ignored) {}
		try { appIcon = require(`../../static/pics/links/${name.toLowerCase()}-logo.svg` ) } catch (ignored) {}

		let appIconHover = appIcon;
		try { appIconHover = require(`../../static/pics/links/${name.toLowerCase()}-logo-gray.jpeg`) } catch (ignored) {}
		try { appIconHover = require(`../../static/pics/links/${name.toLowerCase()}-logo-gray.jpg` ) } catch (ignored) {}
		try { appIconHover = require(`../../static/pics/links/${name.toLowerCase()}-logo-gray.png` ) } catch (ignored) {}
		try { appIconHover = require(`../../static/pics/links/${name.toLowerCase()}-logo-gray.svg` ) } catch (ignored) {}

		return { icon: appIcon, iconHover: appIconHover }
	}

	const apps: Array<ExternalApp> = useMemo(() => {

		const processUrl = (item: _ExternalApp) => {

			if ( item.urlTemplate.includes('${userAddress}') ) {
				if ( userAddress !== undefined ) {
					return item.urlTemplate
						.replaceAll('${contractAddress}', contractAddress)
						.replaceAll('${tokenId}', tokenId)
						.replaceAll('${userAddress}', userAddress)
				}
				if ( item.urlFallback !== undefined ) {
					return item.urlFallback
						.replaceAll('${contractAddress}', contractAddress)
						.replaceAll('${tokenId}', tokenId)
				}

				return item.urlTemplate
					.replaceAll('${contractAddress}', contractAddress)
					.replaceAll('${tokenId}', tokenId)

			}

			return item.urlTemplate
				.replaceAll('${contractAddress}', contractAddress)
				.replaceAll('${tokenId}', tokenId)
		}

		const foundChain = config.CHAIN_SPECIFIC_DATA.find((item) => { return item.chainId === chainId });
		if ( !foundChain || !foundChain.ownerSocials ) { return [] }

		return foundChain.ownerSocials.map((item) => {
			return {
				...item,
				url: processUrl(item),
				...getAppIcon(item.name)
			}
		});

	}, [ chainId ])

	const getItem = (item: ExternalApp) => {
		return (
			<div className="col-lg-4 col-md-6 mb-2 order-1 order-md-2" key={item.name}>
				<a href={ item.url } target="_blank" rel="nofollow noopener noreferrer">
					<div className="tb-coin mb-2">
						<img style={{ width: '24px', height: '24px' }} src={ item.iconHover } alt="" />
						<span className="ml-2">{ item.name }</span>
					</div>
				</a>
			</div>
		)
	}

	const getBody = () => {
		if ( !apps.length ) { return null; }
		if ( isOwnerContract ) { return null; }

		return (
			<div className="c-wrap">
				<div className="c-wrap__header">
					<div className="d-flex align-items-center flex-wrap">
						<div className="h4">Contact with owner</div>
					</div>
				</div>
				<div className="c-wrap__table mt-3 ">
					<div className="item">
						<div className="row">
							{ apps.map((item) => { return getItem(item) }) }
						</div>
					</div>
				</div>
			</div>
		)

	}

	return getBody();

}
