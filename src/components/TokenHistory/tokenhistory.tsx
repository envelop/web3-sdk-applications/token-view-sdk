
import {
	ReactElement,
	useContext,
	useEffect,
	useState
} from "react";
import {
	NFTorWNFT,
	TokenHistoryItem,
	TokenHistoryAction,
	_AssetType,
	combineURLs,
	compactString,
	dateToStr,
	getTokenHistory,
	unixtimeToDate,
	getNullERC20,
	tokenToFloat,
	BigNumber,
	chainTypeToERC20,
} from "@envelop/envelop-client-core";
import {
	ERC20Context,
	Web3Context
} from "../../dispatchers";

import i_transfer    from '../../static/pics/icons/i-transfer.svg';
import i_wrap        from '../../static/pics/icons/i-wrap.svg';
import i_unwrap      from '../../static/pics/icons/i-unwrap.svg';
import icon_external from "../../static/pics/icons/i-external.svg";
import TippyWrapper  from "../TippyWrapper";

type TokenHistoryProps = {
	token: NFTorWNFT
}

export default function TokenHistory(props: TokenHistoryProps) {

	const {
		token,
	} = props;

	const {
		currentChain,
	} = useContext(Web3Context);
	const {
		erc20List,
		requestERC20Token
	} = useContext(ERC20Context);

	const HISTORY_ITEMS_ON_PAGE = 6;
	const [ historyItems,      setHistoryItems      ] = useState<Array<TokenHistoryItem>>([]);
	const [ historyPage,       setHistoryPage       ] = useState<number>(1);

	useEffect(() => {
		if ( !currentChain ) { return; }

		const fetchHistory = async (page: number) => {

			const itemsOnPage = 30;

			try {
				const resp = await getTokenHistory(token.assetType, currentChain.chainId, token.contractAddress, token.tokenId, itemsOnPage, page);
				if ( resp.length === itemsOnPage ) { fetchHistory(page + 1) }

				setHistoryItems((prevState) => {
					return [
						...prevState.filter((item) => {
							const foundInResp = resp.find((iitem) => { return item.blocknumber === iitem.blocknumber && item.logindex === iitem.logindex && item.txhash.toLowerCase() === iitem.txhash.toLowerCase() });
							return !foundInResp
						}),
						...resp
					]
				})
			} catch(e) {
				console.log('Cannot fetch token history', e);
			}
		}

		fetchHistory(1);
	}, [ token ]);

	if ( !historyItems.length ) { return null; }

	const getHistoryPagination = () => {

		const pages = Math.ceil(historyItems.length / HISTORY_ITEMS_ON_PAGE);
		if ( pages === 1 ) { return null; }

		const getPrevBtn = () => {
			const isDisabled = historyPage === 1;
			return (
				<li>
				<button
					className={`arrow ${ isDisabled ? 'disabled' : ''}`} disabled={isDisabled}
					onClick={() => { setHistoryPage(historyPage - 1) }}
				>
					<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path fill-rule="evenodd" clipRule="evenodd" d="M24.0684 10.3225C24.504 10.7547 24.5067 11.4582 24.0744 11.8938L15.9623 20.0679L23.9397 28.1062C24.372 28.5418 24.3693 29.2453 23.9338 29.6775C23.4982 30.1098 22.7947 30.1071 22.3624 29.6716L13.6082 20.8505C13.1783 20.4173 13.1783 19.7184 13.6082 19.2852L22.4971 10.3284C22.9294 9.89287 23.6329 9.89019 24.0684 10.3225Z" fill="white"></path>
					</svg>
				</button>
				</li>
			)
		}
		const getNextBtn = () => {
			const isDisabled = historyPage === pages;
			return (
				<li>
				<button
					className={`arrow ${ isDisabled ? 'disabled' : ''}`} disabled={isDisabled}
					onClick={() => { setHistoryPage(historyPage + 1) }}
				>
					<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path fill-rule="evenodd" clipRule="evenodd" d="M15.9316 29.6775C15.496 29.2453 15.4933 28.5418 15.9256 28.1062L24.0377 19.9321L16.0603 11.8938C15.628 11.4582 15.6307 10.7547 16.0662 10.3225C16.5018 9.89019 17.2053 9.89287 17.6376 10.3284L26.3918 19.1495C26.8217 19.5827 26.8217 20.2816 26.3918 20.7148L17.5029 29.6716C17.0706 30.1071 16.3671 30.1098 15.9316 29.6775Z" fill="white">                   </path>
					</svg>
				</button>
				</li>
			)
		}

		return (
			<div className="pagination">
				<ul>

					{ getPrevBtn() }

					{
						[...Array(pages)].map((_, item) => {
							const page = item + 1;
							return (
								<li>
									<button
										className={page === historyPage ? 'active' : ''}
										onClick={() => { setHistoryPage(page) }}
									>{ page }</button>
								</li>
							)
						})
					}

					{ getNextBtn() }
				</ul>
			</div>
		)
	}

	const wrapItemWithHint = (item: TokenHistoryItem, el: ReactElement) => {
		if ( !item.description ) { return el; }

		return (
			<TippyWrapper msg={ item.description }>{ el }</TippyWrapper>
		)
	}
	const getItemType = (item: TokenHistoryItem) => {
		if ( item.action === TokenHistoryAction.Transfer ) {
			return wrapItemWithHint(item, (
				<div className="tb-activity">
					<img className="icon" src={ i_transfer } alt="" />
					<span>Transfer</span>
				</div>
			))
		}
		if ( item.action === TokenHistoryAction.Sale ) {
			return wrapItemWithHint(item, (
				<div className="tb-activity">
					<img className="icon" src={ i_transfer } alt="" />
					<span>Sale</span>
				</div>
			))
		}
		if ( item.action === TokenHistoryAction.CollateralAdded ) {
			return wrapItemWithHint(item, (
				<div className="tb-activity">
					<img className="icon" src={ i_wrap } alt="" />
					<span>Collateral Added</span>
				</div>
			))
		}
		if ( item.action === TokenHistoryAction.CollateralRemoved ) {
			return wrapItemWithHint(item, (
				<div className="tb-activity">
					<img className="icon" src={ i_unwrap } alt="" />
					<span>Collateral Removed</span>
				</div>
			))
		}
		if ( item.action === TokenHistoryAction.EnvelopFee ) {
			return wrapItemWithHint(item, (
				<div className="tb-activity">
					<img className="icon" src={ i_unwrap } alt="" />
					<span>Envelop Fee</span>
				</div>
			))
		}
		return null;
	}
	const getLink = (item: TokenHistoryItem) => {
		if ( !currentChain ) { return null; }
		if ( item.unixtime ) {
			return (
				<a className="ex-link" target="_blank" rel="noopener noreferrer" href={ combineURLs(currentChain.explorerBaseUrl, `/tx/${item.txhash}`) }>
					<span>{ dateToStr(unixtimeToDate(item.unixtime)) }</span>
					<img className="i-ex" src={ icon_external } alt="" />
				</a>
			)
		}

		return (
			<a className="ex-link" target="_blank" rel="noopener noreferrer" href={ combineURLs(currentChain.explorerBaseUrl, `/tx/${item.txhash}`) }>
				<span>{ item.blocknumber }</span>
				<img className="i-ex" src={ icon_external } alt="" />
			</a>
		)
	}

	const getFrom = (item: TokenHistoryItem) => {
		if (
			item.action !== TokenHistoryAction.Transfer &&
			item.action !== TokenHistoryAction.Sale
		) {
			return (<div className="mb-2 col-6 col-md-2"></div>)
		}
		if ( !item.from ) {
			return (<div className="mb-2 col-6 col-md-2"></div>)
		}

		return (
			<TippyWrapper msg={item.from}>
				<div className="mb-2 col-6 col-md-2">
					{ compactString(item.from) }
				</div>
			</TippyWrapper>
		)
	}
	const getTo = (item: TokenHistoryItem) => {
		if (
			item.action !== TokenHistoryAction.Transfer &&
			item.action !== TokenHistoryAction.Sale
		) {
			return (<div className="mb-2 col-6 col-md-2"></div>)
		}
		if ( !item.to ) {
			return (<div className="mb-2 col-6 col-md-2"></div>)
		}

		return (
			<TippyWrapper msg={item.to}>
				<div className="mb-2 col-6 col-md-2">
					{ compactString(item.to) }
				</div>
			</TippyWrapper>
		)
	}

	const getAmount = (item: TokenHistoryItem) => {

		if ( !currentChain ) { return; }

		if (
			item.action !== TokenHistoryAction.CollateralAdded &&
			item.action !== TokenHistoryAction.CollateralRemoved
		) {
			return (<div className="mb-2 col-6 col-md-2"></div>)
		}
		if ( !item.contractAddress || !item.amount ) {
			return (<div className="mb-2 col-6 col-md-2"></div>)
		}

		let foundERC20 = [
			...erc20List,
			chainTypeToERC20(currentChain)
		].find((iitem) => {
			return item.contractAddress && item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase()
		});
		if ( !foundERC20 ) {
			requestERC20Token(item.contractAddress);
			foundERC20 = getNullERC20(item.contractAddress);
		}

		const amount = tokenToFloat(new BigNumber(item.amount), foundERC20.decimals);

		return (
			<div className="mb-2 col-6 col-md-2">
				<div className="tb-coin">
					<TippyWrapper msg={ foundERC20.symbol }>
						<span className="i-coin">
							<img src={ foundERC20.icon } alt="" />
						</span>
					</TippyWrapper>
					<TippyWrapper msg={amount.toString()}>
						<span>{
							!amount || amount.eq(0) ? '0':
								amount && amount.gte(0.00001) ?
									amount.toFixed(5) : ( '<0.00001' )
						}</span>
					</TippyWrapper>
				</div>
			</div>
		)
	}

	const getHistoryRow = (item: TokenHistoryItem) => {
		return (
			<div className="item" key={`${item.blocknumber}${item.logindex}${item.txhash}`}>
				<div className="row">
					<div className="mb-2 col-6 col-md-3">
						{ getItemType(item) }
					</div>

					{ getFrom(item) }
					{ getTo(item) }

					{ getAmount(item) }

					<div className="mb-2 col-6 col-md-3 md-right">
						{ getLink(item) }
					</div>
				</div>
			</div>
		)
	}

	return (
		<div className="c-wrap">
			<div className="c-wrap__header">
				<div className="h4 mt-2">Activity History</div>
			</div>
			<div className="c-wrap__table mt-3">
				{
					historyItems
						.sort((prev, item) => {
							if ( item.blocknumber === prev.blocknumber ) { return item.logindex - prev.logindex }

							return item.blocknumber - prev.blocknumber
						})
						.slice(
							((historyPage - 1) * HISTORY_ITEMS_ON_PAGE),
							((historyPage - 1) * HISTORY_ITEMS_ON_PAGE) + HISTORY_ITEMS_ON_PAGE
						)
						.map((item: TokenHistoryItem) => {
							return getHistoryRow(item)
						})
				}
			</div>
			{ getHistoryPagination() }
		</div>
	)

}