import {
	BigNumber,
	CollateralItem,
	Fee,
	Lock,
	Price,
	Royalty,
	Rules,
	Web3,
	_Asset,
	_AssetItem,
	_AssetType,
	_Fee,
	_Lock,
	_Royalty,
	createContract,
	decodeAssetTypeFromIndex,
	encodeCollaterals,
	encodeFees,
	encodeLocks,
	encodePrice,
	encodeRoyalties,
	encodeRules,
	getDefaultWeb3,
	getNativeCollateral,
	getUserAddress
} from "@envelop/envelop-client-core";

export const getMaxCollaterals = async (chainId: number, wrapperAddress: string): Promise<number> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'wrapper', wrapperAddress);

	let slots = 25;
	try {
		slots = parseInt(await contract.methods.MAX_COLLATERAL_SLOTS().call());
	} catch(e: any) { throw e; }

	return slots;

}
export const getWrapperWhitelist = async (chainId: number, wrapperAddress: string): Promise<string | undefined> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'wrapper', wrapperAddress);
	try {
		const whitelist = await contract.methods.protocolWhiteList().call();
		if ( !whitelist || whitelist === '' || whitelist === '0x0000000000000000000000000000000000000000' ) { return undefined; }
		return whitelist
	} catch(e: any) { throw e; }

}
export const getWhitelist = async (chainId: number, whitelistAddress: string): Promise<Array<_Asset>> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		const whitelist = await contract.methods.getWLAddresses().call();
		return whitelist.map((item: { contractAddress: string, assetType: string }) => { return { ...item, assetType: decodeAssetTypeFromIndex(item.assetType) } });
	} catch(e: any) { throw e; }

}
export const getWhitelistItem = async (chainId: number, whitelistAddress: string, idx: number): Promise<_Asset> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		return await contract.methods.getWLAddressByIndex(idx).call();
	} catch(e: any) { throw e; }

}
export const getBlacklist = async (chainId: number, whitelistAddress: string): Promise<Array<_Asset>> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		const blacklist = await contract.methods.getBLAddresses().call();
		return blacklist.map((item: { contractAddress: string, assetType: string }) => { return { ...item, assetType: decodeAssetTypeFromIndex(item.assetType) } });
	} catch(e: any) { throw e; }

}
export const isEnabledForCollateral = async (chainId: number, whitelistAddress: string, tokenAddress: string): Promise<boolean> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		return await contract.methods.enabledForCollateral(tokenAddress).call();
	} catch(e: any) { throw e; }

}
export const isEnabledForFee = async (chainId: number, whitelistAddress: string, tokenAddress: string): Promise<boolean> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		return await contract.methods.enabledForFee(tokenAddress).call();
	} catch(e: any) { throw e; }

}
export const isEnabledRemoveFromCollateral = async (chainId: number, whitelistAddress: string, tokenAddress: string): Promise<boolean> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		return await contract.methods.enabledRemoveFromCollateral(tokenAddress).call();
	} catch(e: any) { throw e; }

}

export type _INData = {
	inAsset:_AssetItem,
	unWrapDestinition: string, // fallback for old contracts; will be deprecated
	unWrapDestination: string,
	fees: Array<_Fee>,
	locks: Array<_Lock>,
	royalties: Array<_Royalty>,
	outType: _AssetType,
	outBalance: string,
	rules: string,
}
export type WrapTransactionArgs = {
	_inData: _INData,
	_collateral: Array<_AssetItem>,
	_wrappFor: string,
}
export const encodeINData = (params: {
	originalToken         : {
		assetType: _AssetType,
		contractAddress: string,
		tokenId: string,
		amount: BigNumber,
	},
	unwrapDestination     : string,
	fees                  : Array<Fee>,
	locks                 : Array<Lock>,
	royalties             : Array<Royalty>,
	rules                 : Rules,
	outType               : _AssetType,
	outBalance            : number,
	wrapperContractAddress: string,
}): _INData => {
	return {
		inAsset          : {
			asset: {
				assetType: params.originalToken.assetType,
				contractAddress: params.originalToken.contractAddress || '0x0000000000000000000000000000000000000000',
			},
			tokenId: params.originalToken.tokenId || '0',
			amount: params.originalToken.amount ? params.originalToken.amount.toString() : '0',
		},
		unWrapDestinition: params.unwrapDestination,
		unWrapDestination: params.unwrapDestination,
		fees             : encodeFees(params.fees),
		locks            : encodeLocks(params.locks),
		royalties        : encodeRoyalties(params.royalties, params.wrapperContractAddress),
		outType          : params.outType,
		outBalance       : params.outBalance.toString(),
		rules            : encodeRules(params.rules),
	}
}
export const encodeWrapArguments = (params: {
	originalToken         : {
		assetType: _AssetType,
		contractAddress: string,
		tokenId: string,
		amount: BigNumber,
	},
	unwrapDestination     : string,
	fees                  : Array<Fee>,
	locks                 : Array<Lock>,
	royalties             : Array<Royalty>,
	rules                 : Rules,
	outType               : _AssetType,
	outBalance            : number,
	collaterals           : Array<CollateralItem>,
	wrapFor               : string,
	wrapperContractAddress: string,
}): WrapTransactionArgs => {

	let transferToken = undefined;
	if ( params.fees.length ) {
		transferToken = params.fees[0].token;
	}

	return {
		_inData: encodeINData({ ...params }),
		_collateral: encodeCollaterals(params.collaterals, transferToken),
		_wrappFor: params.wrapFor,
	}
}

export const wrapToken = async (
	web3: Web3,
	wrapperContract: string,
	params: {
		originalToken         : {
			assetType: _AssetType,
			contractAddress: string,
			tokenId: string,
			amount: BigNumber,
		},
		unwrapDestination     : string,
		fees                  : Array<Fee>,
		locks                 : Array<Lock>,
		royalties             : Array<Royalty>,
		rules                 : Rules,
		outType               : _AssetType,
		outBalance            : number,
		collaterals           : Array<CollateralItem>,
		wrapFor               : string,
	}
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'wrapper', wrapperContract);

	const nativeCollateral = getNativeCollateral(params.collaterals);
	const encodedParams = encodeWrapArguments({ ...params, wrapperContractAddress: wrapperContract });
	const tx = contract.methods.wrap(
		encodedParams._inData,
		encodedParams._collateral,
		encodedParams._wrappFor,
	);

	try {
		await tx.estimateGas({ from: userAddress, value: nativeCollateral })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, value: nativeCollateral, maxPriorityFeePerGas: null, maxFeePerGas: null });
}

export const unwrapToken = async (
	web3: Web3,
	wrapperContract: string,
	tokenAddress: string,
	tokenId: string,
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'wrapper', wrapperContract);

	const tx = contract.methods.unWrap(tokenAddress, tokenId);

	let estimatedGas = '1000000';
	try {
		const estimatedGasCalced = await tx.estimateGas({ from: userAddress });
		estimatedGas = new BigNumber(estimatedGasCalced).plus(new BigNumber(100000)).toString();
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, gas: estimatedGas, maxPriorityFeePerGas: null, maxFeePerGas: null });
}
export const unwrapTokenMultisig = async (
	web3: Web3,
	wrapperContract: string,
	tokenAddress: string,
	tokenId: string,
) => {
	return new Promise(async (res, rej) => {
		const userAddress = await getUserAddress(web3);
		if ( !userAddress ) { return; }

		const contract = await createContract(web3, 'wrapper', wrapperContract);

		const tx = contract.methods.unWrap(tokenAddress, tokenId);

		// try {
		// 	await tx.estimateGas({ from: userAddress })
		// } catch(e) {
		// 	throw e;
		// }

		tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null }, (err: any, data: any) => {
			if ( err ) { rej(err); }
			res(data);
		})
	});
}

export enum NFTOrderTypes {
	SELL = 0,
	BUY = 1,
}
export type NFTOrderPrice = {
	payToken: string,
	payAmount: string,
}
export const decodeNFTOrderType = (_type: string | number): NFTOrderTypes => {
	if ( `${_type}` === '0' ) { return NFTOrderTypes.SELL; }
	if ( `${_type}` === '1' ) { return NFTOrderTypes.BUY;  }

	return NFTOrderTypes.SELL;
}
export const encodeNFTOrderType = (_type: NFTOrderTypes): string => {
	if ( _type === NFTOrderTypes.BUY ) { return '0'; }
	if ( _type === NFTOrderTypes.SELL ) { return '1';  }

	return '0';
}
export type NFTOrder = {
	orderId          : string,
	orderType        : NFTOrderTypes,
	orderBook        : string,
	orderMaker       : string,
	amount           : BigNumber,
	price            : NFTOrderPrice,
	assetApproveExist: boolean,
}
export const getNFTOrders = async (chainId: number, orderManagerAddress: string, token: _AssetItem): Promise<Array<NFTOrder>> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'ordermanager', orderManagerAddress);

	let output = []
	try {
		const orders = await contract.methods.getOrdersForItem(token).call();
		output = orders.map((item: any) => {
			return {
				orderId: item.orderId,
				orderType: item.orderType,
				orderBook: item.orderBook,
				orderMaker: item.orderMaker,
				amount: item.amount,
				price: {
					payToken: item.price.payToken,
					payAmount: item.price.payAmount,
				},
				assetApproveExist: item.assetApproveExist,
			}
		})
	} catch(e: any) { throw e; }

	return output;
}
export const makeNFTOrderBuy = async (
	web3: Web3,
	orderManagerAddress: string,
	token: _AssetItem,
	price: NFTOrderPrice,
) => {

	if ( price.payToken.toLowerCase() === '0x0000000000000000000000000000000000000000' ) {
		throw new Error('Cannot create buy order with native price');
	}

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'ordermanager', orderManagerAddress);

	const tx = contract.methods.makeOrder(
		token,
		price,
		NFTOrderTypes.BUY,
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}
export const makeNFTOrderBuyMultisig = async (
	web3: Web3,
	orderManagerAddress: string,
	token: _AssetItem,
	price: NFTOrderPrice,
) => {

	if ( price.payToken.toLowerCase() === '0x0000000000000000000000000000000000000000' ) {
		throw new Error('Cannot create buy order with native price');
	}

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'ordermanager', orderManagerAddress);

	const tx = contract.methods.makeOrder(
		token,
		price,
		NFTOrderTypes.BUY,
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return new Promise((res, rej) => {
		tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null }, (err: any, data: any) => {
			if ( err ) { rej(err); }
			res(data);
		});
	});
}
export const makeNFTOrderSell = async (
	web3: Web3,
	orderManagerAddress: string,
	token: _AssetItem,
	price: NFTOrderPrice,
) => {

	if ( price.payToken.toLowerCase() === '0x0000000000000000000000000000000000000000' ) {
		throw new Error('Cannot create buy order with native price');
	}

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'ordermanager', orderManagerAddress);

	let nativePrice: string = '0';
	if ( price.payToken.toLowerCase() === '0x0000000000000000000000000000000000000000' ) {
		nativePrice = price.payAmount;
	}

	const tx = contract.methods.makeOrder(
		token,
		price,
		NFTOrderTypes.SELL,
	);

	try {
		await tx.estimateGas({ from: userAddress, value: nativePrice })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, value: nativePrice, maxPriorityFeePerGas: null, maxFeePerGas: null });
}
export const makeNFTOrderSellMultisig = async (
	web3: Web3,
	orderManagerAddress: string,
	token: _AssetItem,
	price: NFTOrderPrice,
) => {

	if ( price.payToken.toLowerCase() === '0x0000000000000000000000000000000000000000' ) {
		throw new Error('Cannot create buy order with native price');
	}

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'ordermanager', orderManagerAddress);

	let nativePrice: string = '0';
	if ( price.payToken.toLowerCase() === '0x0000000000000000000000000000000000000000' ) {
		nativePrice = price.payAmount;
	}

	const tx = contract.methods.makeOrder(
		token,
		price,
		NFTOrderTypes.SELL,
	);

	return new Promise((res, rej) => {
		tx.send({ from: userAddress, value: nativePrice, maxPriorityFeePerGas: null, maxFeePerGas: null }, (err: any, data: any) => {
			if ( err ) { rej(err); }
			res(data);
		});
	});
}
export const removeNFTOrder = async (
	web3: Web3,
	orderManagerAddress: string,
	orderId: string,
	token: _AssetItem,
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'ordermanager', orderManagerAddress);

	const tx = contract.methods.removeOrder(
		orderId,
		token,
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}
export const removeNFTOrderMultisig = async (
	web3: Web3,
	orderManagerAddress: string,
	orderId: string,
	token: _AssetItem,
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'ordermanager', orderManagerAddress);

	const tx = contract.methods.removeOrder(
		orderId,
		token,
	);

	return new Promise((res, rej) => {
		tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null }, (err: any, data: any) => {
			if ( err ) { rej(err); }
			res(data);
		});
	});
}
export const takeNFTOrder = async (
	web3: Web3,
	orderManagerAddress: string,
	order: NFTOrder,
	token: _AssetItem,
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'ordermanager', orderManagerAddress);

	let nativePrice = '0';
	if ( order.orderType === NFTOrderTypes.SELL ) {
		if ( order.price.payToken === '0x0000000000000000000000000000000000000000' ) {
			nativePrice = order.price.payAmount;
		}
	}

	const tx = contract.methods.takeOrder(
		order.orderId,
		order.orderBook,
		order.orderType,
		token,
	);

	try {
		await tx.estimateGas({ from: userAddress, value: nativePrice })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, value: nativePrice, maxPriorityFeePerGas: null, maxFeePerGas: null });
}
export const takeNFTOrderMultisig = async (
	web3: Web3,
	orderManagerAddress: string,
	order: NFTOrder,
	token: _AssetItem,
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'ordermanager', orderManagerAddress);

	let nativePrice = '0';
	if ( order.orderType === NFTOrderTypes.SELL ) {
		if ( order.price.payToken === '0x0000000000000000000000000000000000000000' ) {
			nativePrice = order.price.payAmount;
		}
	}

	const tx = contract.methods.makeOrder(
		order.orderId,
		order.orderBook,
		order.orderType,
		token,
	);

	try {
		await tx.estimateGas({ from: userAddress, value: nativePrice })
	} catch(e) {
		throw e;
	}

	return new Promise((res, rej) => {
		tx.send({ from: userAddress, value: nativePrice, maxPriorityFeePerGas: null, maxFeePerGas: null }, (err: any, data: any) => {
			if ( err ) { rej(err); }
			res(data);
		});
	});
}