Inside this `./server` folder

Map `./public` folder to `./server/public`

Build image:
```docker build -f Dockerfile -t tokenviewsdkapp .```

Run container:
```docker run -p 3031:3031 tokenviewsdkapp:latest```