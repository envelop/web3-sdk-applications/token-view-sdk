
import re
import os
import traceback
from datetime import datetime
from dotenv import load_dotenv
load_dotenv()

def format_now():
	return datetime.now().strftime('%Y-%m-%d %H:%M:%S')

url_regex = re.compile('token\/\d*\/0x\w*\/\d*')
remove_old_regexp = re.compile('<meta (name|property)=\"\w*:image\"[^>]*>')

APP_BASE_URL = os.environ.get('APP_BASE_URL', 'https://app.envelop.is')
APP_URL_PREFIX = 'token'
APP_TOKEN_URL = f'{APP_BASE_URL}/{APP_URL_PREFIX}'
IMAGE_URL_PREFIX = os.environ.get('IMAGE_URL_PREFIX', 'https://api.envelop.is:8443')

print('APP_BASE_URL', APP_BASE_URL)
print('IMAGE_URL_PREFIX', IMAGE_URL_PREFIX)

OG_IMAGE_TAGS = [
	'''<meta property="og:url" content="{token_url}">''',
	'''<meta property="og:image" content="{image_url}">''',
	'''<meta property="twitter:domain" content="{APP_BASE_URL}">''',
	'''<meta property="twitter:url" content="{token_url}">''',
	'''<meta name="twitter:image" content="{image_url}">''',
	'''<link rel="preload" as="image" href="/{image_url}" as="image" />''',
]

def application(env, start_response):

	template = None
	template_parts = None
	try:
		f = open('index.html', 'r')
	except FileNotFoundError:
		start_response('500 Internal Server Error', [('Content-Type','text/html')])
		return [b"Cannot open template"]
	else:
		with f:
			template = f.readlines()

	template_parts = remove_old_regexp.sub('', template[0])
	template_parts = template_parts.split('</head>')

	response = []
	try:
		url = env['REQUEST_URI']
		url_parsed = url_regex.search(url).group(0).split('/')
		chain_id = url_parsed[1]
		contract_address = url_parsed[2]
		token_id = url_parsed[3]
		print(f'{format_now()}: Forming index.html for tokenview {chain_id}/{contract_address}/{token_id}. User-Agent: {env["HTTP_USER_AGENT"]}')
	except:
		print(f'{format_now()}: Cannot form index.html for tokenview')
		traceback.print_exc()
		start_response('500 Internal Server Error', [('Content-Type','text/html')])
		return [b"Cannot parse url"]

	token_url = f'{APP_TOKEN_URL}/{chain_id}/{contract_address}/{token_id}'
	image_url = f'{IMAGE_URL_PREFIX}/{chain_id}/{contract_address}/{token_id}/image'

	response.append(bytes(template_parts[0], 'UTF-8'));
	for item in OG_IMAGE_TAGS:
		formatted = item.format(APP_BASE_URL = APP_BASE_URL, token_url = token_url, image_url = image_url)
		response.append(bytes(formatted, 'UTF-8'))
	response.append(bytes('</head>', 'UTF-8'));
	response.append(bytes(template_parts[1], 'UTF-8'));

	start_response('200 OK', [('Content-Type','text/html')])
	return response

